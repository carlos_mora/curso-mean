products = 
{ 
    id: objectid,
    sku: string,
    nombre: string,
    ean: string,
    descripcion: string,
    fotos : {

    }
    atributos:[
        {
            atributo: string,
            valor: string
        }
    ],
    precio: decimal,       
    precios_hist: [ // ***** ver opcional
        {
            precio: number,
            fecha: timestamp,
        }
    ],
    categorias: [ string ];
    reviews : [
        {
            usuario: string,
            comentario: string,
            votos: integer,
            fecha: timestamp,
        }
    ]
}

categorias = {
    _id: oid,
    nombre: string, 
    superior: oid
}


// Opcional: 
precios_hist = {
        _id: oid,
        producto_id: oid,
        precio: number,
        fecha: timestamp
    }

