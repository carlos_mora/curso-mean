db.tweets.aggregate([
    {
        $match: {
            "user.friends_count": { $gt: 0 },
            "user.followers_count": { $gt: 0 }
        }
    },
    {
        $project: {
            ratio: {
                $divide: ["$user.followers_count", "$user.friends_count"]
            },
            screen_name: "$user.screen_name"
        }
    },
    { $sort: { ratio: -1 } },
    { $limit: 1 }
]);

db.tweets.aggregate([
    { $sort: { "user.lang": 1, "user.time_zone": 1 } },
    {
        $group: {
            _id: { lang :"$user.lang" , tz : "$user.time_zone" },
            count: { $sum: 1 }
        }
    },
    { $sort: { count: -1 } }
]);

// importar datos a mongo 
mongoimport -d blog -c posts --drop posts.json

// si estamos limitados de recursos...
mongoimport -d blog -c posts --drop --batchSize=100 posts.json


db.posts.aggregate( [
    { $unwind : "$comments"},
    { $group : { 
        _id: "$comments.author", 
        cuenta: {$sum: 1}, 
        authors:{ $addToSet: "$author"} 
        } 
    },
    { $sort : { "$cuenta": -1}},
    { $limit : 1 }
])

db.zips.aggregate( [
    { $match: [ "state" : {$in : ["CA", "NY"]} ] },
    { $group: { 
        _id : {state:"$state", city: "$city"}, 
        pop:{$sum:"$population"}
        }
    }

    },
])

db.zips.aggregate( [ 
    {
        $match: { 
            state : { $in : ["CA", "NY"] } 
        } 
    } ,
    {
        $group : {
            _id : {state:"$state", city: "$city"}, 
            poblacion: { $sum:"$pop"}
        }
    }, 
    {
        $match : {
            poblacion : { $gt : 25000}
        }
    },
    {
        $group : {
            _id: { },
            promedio : { $avg : "$poblacion"}
        }
    }
] )

