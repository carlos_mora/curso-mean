//
//Typescript es un lenguaje tipado (pero no fuertemente tipado)
//
var numero = 666;
var texto = "En la tumba sin nombre junto a la de Stanton";
var action = true; //false 
var noSe;
//Como Typescript se transpila a JS podemos escribir directamente JS:
var noseque = "nosecuantos";
//Arrays
var palabras = ["un", "dos", "tres", "picadora", "moulinex"];
function saludar() {
    console.log("Hola mundo, ahora con typescript");
}
function sumar(s1, s2) {
    return s1 + s2;
}
saludar();
var suma = sumar(10, 500);
