var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Persona = /** @class */ (function () {
    function Persona() {
    }
    // modificadores de acceso public / protected / provate si no pones nada es
    // publi
    Persona.prototype.setId = function (id) {
        this.id = id;
    };
    Persona.prototype.getId = function () {
        return this.id;
    };
    Persona.prototype.setTelefono = function (telefono) {
        this.telefono = telefono;
    };
    Persona.prototype.getTelefono = function () {
        return this.telefono;
    };
    Persona.prototype.toString = function () {
        return this.getId() + ','
            + this.nombre + ','
            + this.telefono + ','
            + this.direccion;
    };
    return Persona;
}());
// Modelo anemico: tienen estado pero no funcionalidad relaciones de herencia
// relaciones de composicion : de composicion y de uso jerarquia de entidades
var Cliente = /** @class */ (function (_super) {
    __extends(Cliente, _super);
    function Cliente() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Cliente.prototype.toString = function () {
        return _super.prototype.toString.call(this) + ','
            + this.cuentaBancaria;
    };
    return Cliente;
}(Persona));
var persona = new Persona();
/*
persona.id = 5;
persona.nombre = "John McCLane";
persona.telefono = "555";
*/ 
