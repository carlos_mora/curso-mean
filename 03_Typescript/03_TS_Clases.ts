class Persona {
	private id: number;
	// public nombre: string;
	//protected direccion: string;
	// private telefono: string;

	// modificadores de acceso public / protected / provate si no pones nada es
	// public

	constructor(id: number, private nombre: string, protected direccion: string, private telefono: string) {
		this.setId(id);
		this.nombre = nombre;
		this.direccion = direccion;
		this.setTelefono(telefono);
	}

	public setId(id: number): void {
		this.id = id;
	}

	public getId(): number {
		return this.id;
	}

	public setTelefono(telefono: string): void {

		this.telefono = telefono;
	}

	public getTelefono(): string {
		return this.telefono;
	}
	public toString(): string {
		return this.getId() + ',' +
			this.nombre + ',' +
			this.telefono + ',' +
			this.direccion;
	}

}

// Modelo anemico: tienen estado pero no funcionalidad relaciones de herencia
// relaciones de composicion : de composicion y de uso jerarquia de entidades

class Cliente extends Persona {
	private cuentaBancaria: string;
	constructor(id: number, nombre: string, direccion: string, telefono: string, ctaBancaria: string) {
		super(id, nombre, direccion, telefono);
		this.cuentaBancaria = ctaBancaria;
	}

	public toString(): string {
		return super.toString() + ',' +
			this.cuentaBancaria;
	}

}

let persona: Persona = new Persona(1, 'Pepe', '45 Terrace St', '9090129012');

/*
persona.id = 5;
persona.nombre = "John McCLane";
persona.telefono = "555";
*/