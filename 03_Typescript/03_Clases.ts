class Persona {

    public nombre:string;
    public direccion:string;
    public telefono:number;

    constructor(nombre:string, direccion:string, telefono:number){
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    } 

    public imprimir():void{
        console.log(this.nombre+","+this.direccion+","+this.telefono);
    }

    public toString():string{
        return this.nombre+","+this.direccion+","+this.telefono;
    }

}

class Cliente extends Persona{

    public cuentaBancaria : string;

    constructor(nombre:string, direccion:string, telefono:number,cuentaBancaria:string){
        super(nombre, direccion,telefono);
        this.cuentaBancaria = cuentaBancaria;
    } 

    public imprimir():void{
        console.log(this.nombre+","+this.direccion+","+this.telefono+","+this.cuentaBancaria);
    }

    public toString():string{
        return super.toString()+","+this.cuentaBancaria;
    }

}

let persona:Persona = new Persona("Bud Spencer","C/Tal",555);
persona.imprimir();
console.log(persona.toString());

let cliente:Cliente = new Cliente("Bud Spencer","C/Tal",555,"123456");
cliente.imprimir();
console.log(cliente.toString());

//Polimorfismo:
let p:Persona = new Cliente("","",0,"");
//let c:Cliente = new Persona("","",0);





