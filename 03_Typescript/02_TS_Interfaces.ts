
interface Coche {
    marca     : string,
    modelo    : string,
    matricula : string
}

function insertarCoche(coche:Coche):void{
    console.log(`marca:${coche.marca}, modelo:${coche.modelo}, matrícula:${coche.matricula}`);
}

let coche:Coche = { 'marca'     : 'Citroën', 
                    'modelo'    : 'Ami 6',
                    'matricula' : 'M-800000' };

insertarCoche(coche);

////////////////////////////////

let pelicula = { 'titulo' : 'Elbueno, el feo y el malo' };
//insertarCoche(pelicula);
                    

