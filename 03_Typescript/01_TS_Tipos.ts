//
//Typescript es un lenguaje tipado (pero no fuertemente tipado)
//
let numero : number = 666;
let texto  : string = "En la tumba sin nombre junto a la de Stanton";
let action : boolean = true; //false 
let noSe   : any;

//Como Typescript se transpila a JS podemos escribir directamente JS:
let noseque = "nosecuantos";

//Arrays
let palabras : string[] = [ "un", "dos", "tres", "picadora", "moulinex" ];

function saludar():void{
    console.log("Hola mundo, ahora con typescript");
}

function sumar(s1:number, s2:number):number{
    return s1+s2;
}

saludar();
let suma:number = sumar(10,500);
console.log(suma);
