const express = require("express");
const mongo = require("mongodb");
const bodyParser = require("body-parser");
const auth = require('basic-auth');
const app = express();

process.on('exit', function() {
    console.log('Adiós mundo cruel...');
    mongoDBUtil.cerrarBBDD();
})

app.use(bodyParser.json());
app.disable("x-powered-by");

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
});

app.get('/aviones', function(req, res) {
    comprobarCredenciales(req, res)
    res.sendStatus(200)
});

app.get('/logout', function(req, res) {
    res.sendStatus(401);
});

app.listen(4321, function() {
    console.log("Esperando peticiones...");
});


function comprobarCredenciales(req, res, callback) {

    let credenciales = auth(req);
    console.log("==============================");

    console.log(credenciales);
    if (!credenciales) {
        res.setHeader('WWW-Authenticate', 'Basic realm=ejemplo');
        res.sendStatus(401);
        return false;
    }

    mongo.connect("mongodb://localhost:27017/incidencias", function(err, db) {
        if (err) { throw (err) };
        let criterio = {
            login: credenciales.name,
            pw: credenciales.pass
        };

        db.collection("usuarios").findOne(criterio, function(err, usuario) {
            if (err) { throw (err) };
            if (!usuario) {
                res.setHeader('WWW-Authenticate', 'Basic realm=ejemplo');
                res.sendStatus(401);
                return false;
            }
            callback(usuario);
        });

    });
    /*
    if (credenciales.name != 'a' || credenciales.pass != 'a') {
        res.setHeader('WWW-Authenticate', 'Basic realm=ejemplo');
        res.sendStatus(401);
        return false;
    }
    */
}