let aviones = [];

aviones.push({
    id: 1,
    fabricante: "General Dynamics",
    modelo: 'F-16',
    year: 1978
});
aviones.push({
    id: 2,
    fabricante: "Airbus",
    modelo: '380',
    year: 2010
});
aviones.push({
    id: 3,
    fabricante: "Grumman",
    modelo: 'F-14',
    year: 1970
});

let contador = aviones.length;

exports.listarAviones = function () {
    // console.log("NegocioAviones: listarAviones");

    return aviones;
}

exports.insertarAvion = function (avion) {
    avion.id = ++contador;
    aviones.push(avion);
    // console.log("NegocioAviones: insertarAvion");
}

exports.buscarAvion = function (id) {

    for (let i = 0; i < aviones.length; i++) {
        avion = aviones[i];
        if (avion.id == id) {
            return avion;
        }
    };

}

exports.modificarAvion = function (avion) {
    for (let i = 0; i < aviones.length; i++) {
        if (avion.id == aviones[i].id) {
            console.log(avion);
            aviones[i] = avion;
            return;
        }
    };
}
exports.borrarAvion = function (id) {
    for (let i = 0; i < aviones.length; i++) {
        avion = aviones[i];
        if (avion.id == id) {
            aviones.splice(i,1);
            return;
        }
    };

}