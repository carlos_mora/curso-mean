console.log("wiskas");

// añadimos los módulos que necesitamos
// el módulo se guarda en una variable que se utiliza para acceder a las funciones
let http = require("http");

/*

// creamos un servidor
// se provee la funcion que procesa las peticiones
let servidor = http.createServer( function(){
    console.log('Petición recibida');
});
// arrancamos el servidor para que escuche en el puerto 3000
servidor.listen(3000);
*/

// la funcion que propocionamos procesa las peticiones devolviendo una rta
// Nos entrega por parametro la peticion y la respuesta en blanco
http.createServer( function(request, response){
    console.log('---------------------------------');
    console.log(request);
    
    // Configurar la cabecera de la respuesta
    // writeHead( responseCode)
    // response.writeHead(200, {'content-type' : 'text/plain'});
    // response.end("HolaManola");
    response.writeHead(200, {'content-type' : 'text/html'});
    response.end("<script>alert('Hola');</script><h1>HolaManola</h1>");

}).listen(3000);

console.log('esperando peticiones');