console.log('Iniciando...');

const express = require('express');

let app = express();

app.listen(5555, function () {
    //  Se ejecuta cuando el servidor esté iniciado
    console.log("Esperando peticiones...");
});

app.disable('x-powered-by');

// Recursos estáticos
app.use(express.static('ficherosExpress')); // el parámetro es la carpeta donde están los archivos que se sirven estáticamente

// cuando llegue una peticion get con la URL
// el request y el response no son como los de node.js
app.get("/", function (request, response) {
    console.log('Petición recibida.');
    response.send('Ola ke ase');
});

// Parámetros en la petición queryparam
app.get("/sumar", function (request, response) {
    console.log('Petición recibida.');
    let sumando1 = 1 * request.query.s1;
    let sumando2 = 1 * request.query.s2;

    response.send('Ola ke ase ' + (sumando1 + sumando2));
});

// Parámetros en la url pathparam
app.get("/peliculas/directores/:director/generos/:genero", function (request, response) {
    console.log('Petición recibida 333.');
    let director = request.params.director;
    let genero = request.params.genero;

    response.send('Ola ke ase ' + director + ' ' + genero);
});


// Acceder al body en formato JSON 
// Hay bodyparsers para diferentes formatos
let bodyParser = require("body-parser");
app.use(bodyParser.json());


app.post("/coches", function (request, response) {
    console.log("===========================================");
    console.log('Insertar coche');
    let coche = request.body; // esto ya está parseado.
    console.log(coche.marca + ' ' + coche.modelo);
    response.sendStatus(204);
    response.send();
});

app.get("/coches", function (request, response) {
    let coche = {
        'marca': 'Simca',
        'modelo': '1200 Ti',
        'matricula': 'M-2000-BG'
    };
    response.json(coche);

});