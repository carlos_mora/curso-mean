const fs = require("fs");
const http = require("http");
const negocioAviones = require("./04_NegocioAviones.js"); // Ruta Relativa

http.createServer(procesarPeticion).listen(3333);

function procesarPeticion(request, response) {
    let url = request.url;

    console.log(request.method + ' ' + url);

    regexp = new RegExp('^/aviones/[0-9]*$');

    if (request.method == 'GET' && request.url == '/aviones') {
        listarAviones(request, response);
    } else if (request.method == 'GET' && request.url.match(regexp) != null) {
        buscarAvion(request, response);
    } else if (request.method == 'POST' && request.url == '/aviones') {
        insertarAvion(request, response);
    } else if (request.method == 'PUT' && request.url == '/aviones') {
        modificarAvion(request, response);
    } else if (request.method == 'DELETE' && request.url.match(regexp) != null) {
        borrarAvion(request, response);
    } else {
        leerRecursoEstatico(request, response);
    }

}



function leerRecursoEstatico(request, response) {
    fs.readFile('ficheros' + request.url, function (error, data) {
        if (error != null) {
            return404(request, response)
            return;
        }
        response.writeHead(200, {
            'content-type': getMimeFromURL(request.url)
        })
        response.end(data.toString());

    });
};

function return404(request, response) {
    response.writeHead(404, {
        'content-type': 'text/html'
    })
    response.end(request.url + ' no encontrado');
}

function fextension(archivo) {
    if (archivo.lastIndexOf(".") > 0) {
        return archivo.substr(archivo.lastIndexOf("."));
    }
    return '';
}

function getMimeFromURL(url) {
    try {
        return {
            'html': 'text/html',
            'css': 'text/css',
            'txt': 'text/plain',
            'js': 'application/javascript',
        }[fextension(url)];
    } catch (e) {
        return 'text/plain';
    }
}


// 1 recoger cualquier dato o parametros que venga con la petición.
// 2 transformar, si es necesario algo que entienda la capa del modelo
// 3 llamar al metodo de negocio adecuado
// 4 si se devuelve algo, transformarlo al formato adecuado
// 5 Entregar la respuesta

function listarAviones(request, response) {
    let aviones = negocioAviones.listarAviones();
    response.writeHead(200, {
        'content-type': 'application/json'
    });
    response.end(JSON.stringify(aviones));
}

function insertarAvion(request, response) {
    let avion = null;
    console.log('estamos');
    request.on('data', function (datos) {
        console.log(datos.toString);
        avion = JSON.parse(datos);
    });
    request.on('end', function () {
        negocioAviones.insertarAvion(avion);
        response.writeHead(200, {
            'content-type': 'text/plain'
        });
        response.end();
    });
}

function buscarAvion(request, response) {
    let id = request.url.split('/')[2];
    console.log(id);
    let avion = negocioAviones.buscarAvion(id);
    console.log(avion);

    response.writeHead(200, {
        'content-type': 'application/json'
    });
    response.end(JSON.stringify(avion));
}

function modificarAvion(request, response) {
    let avion = null;
    request.on('data', function (datos) {
        console.log(datos.toString);
        avion = JSON.parse(datos);
    });
    request.on('end', function () {
        negocioAviones.modificarAvion(avion);
        response.writeHead(200, {
            'content-type': 'text/plain'
        });
        response.end();
    });
}

function borrarAvion(request, response){
    let id = request.url.split('/')[2];
    let avion = negocioAviones.borrarAvion(id);
    response.end();
}
/*

Tres capas Logica de control, modelo de negocio, persistencia




*/