const mongo = require("mongodb");
const url = "mongodb://localhost:27017/usuarios";

const mongoDBUtil = require('./09_MongoDB_Util.js');


// mongoDBUtil.conectarBBDD();

exports.buscarPorLogin = function (login, pw) {
    let criterio = {
        'login': login,
        'pw': pw
    };
    return mongoDBUtil.getBBDD().collection("usuarios").findOne(criterio);
};

exports.insertarUsuario = function (usuario) {
    return mongoDBUtil.getBBDD().collection("usuarios").insertOne(usuario)
        .then(function (resultado) {
            console.log('insertado ' + resultado.insertedId);
            return resultado.insertedId;
        });
};

exports.modificarUsuario = function (usuario) {
    let criterio = {
        'id': usuario.id
    };
    let actualiza = {
        $set: usuario
    }
    return mongo.connect(url).then(function (db) {
        return db.collection("usuarios").updateOne(criterio, actualiza);
    })
};

exports.borrarUsuario = function (id) {};