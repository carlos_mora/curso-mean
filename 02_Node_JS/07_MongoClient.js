let mongo = require("mongodb");
const url = "mongodb://localhost:27017/usuarios";

// Conectando/creando una bbdd
mongo.connect(url, function (err, db) {
    if (err) {
        throw err;
    }
    // console.log(db);
    // cuando hayamos terminado de usar la bbdd la cerramos
    db.close();

});

// creando una colección
mongo.connect(url, function (err, db) {
    if (err) {
        throw err
    }

    db.createCollection("clientes", function (err, rs) {
        if (err) {
            throw err
        }
        // console.log(rs);
        db.close();
    })
});

/*
// insertar un documento
mongo.connect(url, function (err, db) {
    let cliente = {
        nombre: 'Ringo',
        direccion: 'C/False, 123'
    };
    let col = db.collection("clientes");
    col.insertOne(cliente, function (err, rs) {
        if (err) {
            throw err
        }
        console.log("----------------------------------")
        console.log('insertado el documento');
        db.close();
    })
});
*/
// insertar varios
mongo.connect(url, function (err, db) {
    let clientes = [{
            nombre: 'Ringo',
            direccion: 'C/False, 123'
        },
        {
            nombre: 'Totti',
            direccion: 'C/False, 123'
        },
        {
            nombre: 'Mandrake',
            direccion: 'C/False, 123'
        },
        {
            nombre: 'Tolai',
            direccion: 'C/False, 123',
            asd: 'werji'
        },
    ];
    let col = db.collection("clientes");
    col.insertMany(clientes, function (err, rs) {
        if (err) {
            throw err
        }
        console.log("----------------------------------")
        console.log('insertado documentos');
        console.log(rs.insertedIds);
        db.close();
    })
});

/*
// buscar uno
mongo.connect(url, function (err, db) {
    let col = db.collection("clientes");
    col.findOne({
        'nombre': 'Tolai'
    }, function (err, rs) {
        if (err) {
            throw err
        }
        console.log("----------------------------------")
        console.log('encontrando un  documento');
        console.log(rs);
        // si no encuentra nada rs es null

        db.close();
    })
});

mongo.connect(url, function (err, db) {
    let col = db.collection("clientes");
    // let orden = { nombre: 1 } // por nombre descendente
    let orden = {
        nombre: -1
    } // por nombre descendente
    col.find({
        'nombre': 'Ringo'
    }, {
        nombre: 1
    }).sort(orden).toArray(function (err, rs) {
        if (err) {
            throw err
        }
        console.log("----------------------------------")
        console.log('buscando varios documentos');
        console.log(rs);
        db.close();
    })
});

// Update
// buscar uno

mongo.connect(url, function (err, db) {
    if (err) {
        throw err
    }
    let col = db.collection("clientes");
    col.updateOne({
            'nombre': 'Totti'
        }, {
            $set:{direccion: 'Nueva calle'}
        },
        function (err, rs) {
            if (err) {
                throw err
            }
            console.log("----------------------------------")
            console.log('modificado1 un  documento');
            console.log(rs.result);
            // si no encuentra nada rs es null

            db.close();
        })
});

*/

mongo.connect(url, function (err, db) {
    if (err) {
        throw err
    }

    let col = db.collection("clientes");
    let criterio = {'nombre': 'Ringo'};
    
    col.deleteOne( criterio,
        function (err, rs) {
            if (err) {
                throw err
            }
            console.log("----------------------------------")
            console.log('borrado un  documento');
            console.log(rs.result);
            // si no encuentra nada rs es null

            db.close();
        })
});

mongo.connect(url, function (err, db) {
    if (err) {
        throw err
    }

    db.dropCollection('clientes',
        function (err, rs) {
            if (err) {
                throw err
            }
            console.log("----------------------------------")
            console.log('borrado un  documento');
            console.log(rs);
            // si no encuentra nada rs es null

            db.close();
        })
});

