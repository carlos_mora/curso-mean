let fabricantes = [];

fabricantes.push({
    id: 1,
    nombre: "General Dynamics",
    pais: 'Estados Unidos'
});
fabricantes.push({
    id: 2,
    nombre: "Airbus",
    pais: 'Europa'
});
fabricantes.push({
    id: 3,
    nombre: "Grumman",
    pais: 'Estados Unidos'
});

let contador = fabricantes.length;

exports.listarfabricantes = function () {
    return fabricantes;
}

exports.insertarFabricante = function (fabricante) {
    fabricante.id = ++contador;
    fabricantes.push(fabricante);
}

exports.buscarFabricante = function (id) {

    for (let i = 0; i < fabricantes.length; i++) {
        fabricante = fabricantes[i];
        if (fabricante.id == id) {
            return fabricante;
        }
    };

}

exports.modificarFabricante = function (fabricante) {
    for (let i = 0; i < fabricantes.length; i++) {
        if (fabricante.id == fabricantes[i].id) {
            fabricantes[i] = fabricante;
            return;
        }
    };
}

exports.borrarFabricante = function (id) {
    for (let i = 0; i < fabricantes.length; i++) {
        fabricante = fabricantes[i];
        if (fabricante.id == id) {
            fabricantes.splice(i,1);
            return;
        }
    };
}