let fs = require("fs");
let http = require("http");
let servidor = http.createServer(function(request, response){
    let url = request.url;

    console.log(request.method+' '+url);

    leerRecursoEstatico(request, response);

    
}).listen(1100);



function leerRecursoEstatico(request, response){
        fs.readFile('ficheros'+request.url, function( error, data ){
            if(error!= null) {
                return404(request, response)
                return;
            } 
            response.writeHead(200, {'content-type' : getMimeFromURL( request.url )})
            response.end(data.toString());
            
        });
};

function return404(request, response){
    response.writeHead(404, {'content-type' : 'text/html'})
    response.end(request.url+' no encontrado');
}

function fextension(archivo) {
    if (archivo.lastIndexOf(".") > 0) {
        return archivo.substr(archivo.lastIndexOf("."));
    }
    return '';
}

function getMimeFromURL( url ){
    try{
        return { 
            'html' : 'text/html',
            'css' : 'text/css',
            'txt' : 'text/plain',
            'js' : 'application/javascript',
        }[fextension(url)];
    }
    catch (e){
        return 'text/plain';
    }
}