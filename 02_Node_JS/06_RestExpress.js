console.log('Iniciando...');

const express = require('express');
const negocioAviones = require("./04_NegocioAviones.js"); // Ruta Relativa

let app = express();
let bodyParser = require("body-parser");

app.listen(3333, function () {
    //  Se ejecuta cuando el servidor esté iniciado
    console.log("Esperando peticiones...");
});

// let undefined = 'algo';

app.disable('x-powered-by');
app.use(bodyParser.json());

// Recursos estáticos
app.use(express.static('06_ficheros')); // el parámetro es la carpeta donde están los archivos que se sirven estáticamente

app.get("/aviones", function (request, response) {
    let aviones = negocioAviones.listarAviones();
    response.json(aviones);
});
// Parámetros en la url pathparam
app.get("/aviones/:id", function (request, response) {
    let id = request.params.id;
    let avion = negocioAviones.buscarAvion(id);
    if (avion == undefined) {
        response.sendStatus(404);
    } else {
        response.json(avion);
    }
});

app.post("/aviones", function (request, response) {
    let avion = request.body;
    negocioAviones.insertarAvion(avion);
    response.sendStatus(201);
    // response.send();
});

app.put("/aviones", function (request, response) {
    let avion = request.body;
    negocioAviones.modificarAvion(avion);
    response.sendStatus(204);
});

app.delete("/aviones/:id", function (request, response) {
    negocioAviones.borrarAvion(request.params.id);
    response.sendStatus(204);
});