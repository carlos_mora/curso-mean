let fs = require("fs");

console.log('---------------------------------');

// leyendo Sincrono e hilo es el mismo que lee el archivo
let data = fs.readFileSync("ficheros/texto.txt");
console.log(data.toString());

// Leyendo asincronamente
console.log('---------------------------------');
fs.readFile("ficheros/texto.txt", function( error, data ){
    console.log("Fichero leído");
    console.log(data);
});

console.log("FIN");
