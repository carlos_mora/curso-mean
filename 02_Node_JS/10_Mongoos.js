const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const url = 'mongodb://localhost:27017/bbdd';

mongoose.connect(url, {
    useMongoClient: true
});

let Schema = mongoose.Schema;

// Schema es una funcion
// console.log(Schema);

let cocheSchema = new Schema({
    // _id: { type: String, required: true, unique: true },
    marca: String,
    modelo: String,
    potencia: Number
});

let Coche = mongoose.model('coches', cocheSchema);

let c1 = new Coche({
    marca: "Seat",
    modelo: "1430",
    potencia: 95
});

console.log(c1);

/*
c1.save(err => {
    if (err) {
        throw err
    }
    console.log("coche insertado");
});
*/

let c2 = Coche.findById("5a032574cbdfec158473899d");
// ó

Coche.findById("5a032574cbdfec158473899d", function(err, coche){
    console.log(coche);
})

let criterio = { marca : "Seat" };

Coche.find(criterio, function(err, coches){
    if(err) {throw err }
    console.log(coches);
})

// modificaciones sin tener el objeto
let promesa = Coche.findById("5a032574cbdfec158473899d");
promesa.then( coche => {
    // valoramos si hay que modificar
    // Si si
    coche.potencia += 20;
    coche.save(); // 

}).catch(err){
    throw err;
}

// Coche.findByIdAndUpdate( _id, {nuevos: valores}, callback);
// Coche.findOneAndUpdate( {criterio},{nuevos: valores}, callback);


// funciones para eliminar sin tener antes el objetivo

// Coche.findByIdAndRemove( _id, {nuevos: valores}, callback);

// borrar con el objetivo
let promesa = Coche.findById("5a032574cbdfec158473899d");
promesa.then( coche => {
    // valoramos si hay que borrar
    // Si si
    // coche.remove(); 

}).catch(err){
    throw err;
}


