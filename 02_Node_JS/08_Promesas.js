let fs = require('fs');
let mongo = require('mongodb');
let url = "mongodb://localhost:27017/bbdd";

fs.readFile("08_MongoClient.js", function (err, data) {

});

// let p = new Promise();

let promesa = mongo.connect(url);

// Funciones de una promesa
// promesa.then() cuando se cumple
// promesa.catch() cuando falla

promesa
    .then(function (db) {
        console.log('connesso');
        console.log(db);
    })
    .catch(function (err) {
        console.log('error al conectar');
    })



// Creando promesas   
let promesa2 = new Promise(function (resolve, reject) {
    // Aqui va el codigo a ejecutar


})

function listarClientes() {
    return mongo.connect(url)
        .then(function (db) {
            return db.collection("clientes").find({}).toArray();
        })
        .then(function (clientes) {
            return clientes;
        });
}

// ListarClientes devuelve una promesa

promesa = listarClientes();
promesa.then(function(clientes){
    console.log(clientes.length);
})