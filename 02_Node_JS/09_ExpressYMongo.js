const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const negocio = require('./09_Negocio.js');
const mongoDBUtil = require('./09_MongoDB_Util.js');

process.on('exit', function(){
    console.log('Adiós mundo cruel...');
    mongoDBUtil.cerrarBBDD();
})

mongoDBUtil.conectarBBDD(function (resultado) {
    if(resultado) {
    app.listen(4321, function () {
        console.log("Esperando peticiones...");
    });
}else {
    process.exit(1);
}
});

app.use(bodyParser.json());
app.disable("x-powered-by");
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.header(
        "Access-Control-Allow-Methods",
        "POST, GET, PUT, DELETE, OPTIONS"
    );
    next();
});


app.get("/usuarios/credenciales", function (request, response) {
    let login = request.query.login;
    let pw = request.query.pw;
    negocio.buscarPorLogin(login, pw)
        .then(usuario => {
            if (usuario) {
                response.json(usuario);
            } else {
                response.sendStatus(404);
            }
        })
        .catch(err => response.sendStatus(500));
});

app.post("/usuarios", function (request, response) {
    negocio.insertarUsuario(request.body)
        .then(x => response.sendStatus(200))
        .catch(err => response.sendStatus(500));

});
app.put("/usuarios", function (request, response) {
    let usuario = response.json();
});

app.delete("/usuarios/:id", function (request, response) {

});