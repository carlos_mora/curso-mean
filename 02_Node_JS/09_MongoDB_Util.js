const mongo = require("mongodb");
const url = "mongodb://localhost:27017/usuarios";

let bbdd;

exports.conectarBBDD = function (callback) {
    mongo.connect(url, function(err, db){
        if(err){
            console.log("No se ha podido conectar a la bbdd...");
            callback(false);
            return;
        }
        bbdd = db;
        callback(true);
    });
}

exports.getBBDD = function () {
    return bbdd;
}

exports.cerrarBBDD = function(){
    if(bbdd) {
        console.log("Cerrando la bbdd...");
        bbdd.close();
    }
}