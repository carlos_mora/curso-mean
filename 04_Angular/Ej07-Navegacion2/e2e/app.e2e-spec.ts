import { Ej07Navegacion2Page } from './app.po';

describe('ej07-navegacion2 App', function() {
  let page: Ej07Navegacion2Page;

  beforeEach(() => {
    page = new Ej07Navegacion2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
