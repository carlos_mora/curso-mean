import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './componentes/login/login.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { CuentaComponent } from './componentes/cuenta/cuenta.component';
import { AplicacionComponent } from './componentes/aplicacion/aplicacion.component';

const rutasAplicacion: Routes = [
    {
        path: 'inicio',
        component: InicioComponent
    },
    {
        path: 'cuenta',
        component: CuentaComponent
    }
];
const rutas: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'aplicacion',
        component: AplicacionComponent,
        children: rutasAplicacion
    }
];

@NgModule({
    imports: [RouterModule.forRoot(rutas)],
    exports: [RouterModule]
})
export class AppRutasModule {}
