import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Router, Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './componentes/login/login.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { CuentaComponent } from './componentes/cuenta/cuenta.component';
import { AplicacionComponent } from './componentes/aplicacion/aplicacion.component';
import { AppRutasModule } from 'app/app.rutas.module';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        InicioComponent,
        CuentaComponent,
        AplicacionComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRutasModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
