import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LibrosFormularioComponent } from './componentes/libros-formulario/libros-formulario.component';
import { LibrosListadoComponent } from './componentes/libros-listado/libros-listado.component';
import { LibrosService } from 'app/servicios/libros.service';

@NgModule({
  declarations: [
    AppComponent,
    LibrosFormularioComponent,
    LibrosListadoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    RouterModule.forRoot([
      {
        'path' : '', //Ruta por defecto
        'component' : LibrosListadoComponent
      },
      {
        'path' : 'librosListado', 
        'component' : LibrosListadoComponent
      },
      {
        'path' : 'librosFormulario/:id',
        'component' : LibrosFormularioComponent 
      }
    ])

  ],
  providers: [ LibrosService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
