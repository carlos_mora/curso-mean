//Los nombres delas clases en upper camel case
export class Libro{

    constructor(public id:number,
                public titulo:string,
                public autor:string,
                public ISBN:string,
                public precio:number){
    }

    //toString
    public toString():string{
        return this.id+", "+this.titulo+", "+this.autor+", "+this.ISBN+", "+this.precio;
    }

}

