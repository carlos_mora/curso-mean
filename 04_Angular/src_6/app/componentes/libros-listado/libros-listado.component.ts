import { Component, OnInit } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';
import { LibrosService } from 'app/servicios/libros.service';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Component({
  selector: 'app-libros-listado',
  templateUrl: './libros-listado.component.html',
  styleUrls: ['./libros-listado.component.css']
})
export class LibrosListadoComponent implements OnInit {

  public libros:Libro[];

  constructor(private librosService:LibrosService,
              private http:Http) { 
    this.refrescarTabla();
  }
  
  ngOnInit() {
  }
  
  public refrescarTabla():void{
    //
    //la llamada es asíncrona
    /*
    let that = this;
    this.http.get("http://localhost:1234/libros").
      map(function (respuesta){
        console.log("=====================================");
        console.log(respuesta);
        console.log(respuesta.text());
        console.log(respuesta.json());
        return respuesta.json();
      }).
      subscribe(function(data){
                  console.log("=====================================");
                  console.log(data);
                  that.libros = data;
                },
                function(error){
                  console.log("=====================================");
                  console.log("ZAS:"+error);                  
                },
                function(){
                  console.log("=====================================");
                  console.log("FIN");                  
                }
      );*/

    /*Con expresiones lambda no es necesario lo del 'that'    
    this.http.get("http://localhost:1234/libros").
      map( respuesta => respuesta.json() ).
      subscribe( data => this.libros = data, //<-----
                 error => console.log("ZAS:"+error),
                 () => console.log("FIN") ); */
      
    //El servicio devuelve un observable al que nos subscribimos
    this.librosService.listarLibros().
          subscribe( data  => this.libros = data,
                     error => console.log("ZAS:"+error));
    
  }

}
