import { Component, OnInit } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';
import { LibrosService } from 'app/servicios/libros.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-libros-formulario',
  templateUrl: './libros-formulario.component.html',
  styleUrls: ['./libros-formulario.component.css']
})
export class LibrosFormularioComponent implements OnInit {

  public libroSel:Libro;

  constructor(private librosService:LibrosService,
              private router:Router,
              private activatedRoute:ActivatedRoute) { 
    
    this.libroSel = new Libro(null, null, null, null, null);

    let id:number = activatedRoute.snapshot.params['id'];
    if(id != -1) {
      librosService.buscarLibro(id).
        subscribe( data  => this.libroSel = data,
                   error => console.log );
    }

  }

  ngOnInit() {
  }

  public insertarLibro(){
    //this.librosService.insertarLibro(this.libroSel);
    this.router.navigate([ '/librosListado' ]);
  }

  public modificarLibro(){
    //this.librosService.modificarLibro(this.libroSel);
    this.router.navigate([ '/librosListado' ]);
  }
  
  public borrarLibro(){
    //this.librosService.borrarLibro(this.libroSel);
    this.router.navigate([ '/librosListado' ]);
  }

}
