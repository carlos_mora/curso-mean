import { Injectable } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class LibrosService {

  private url:string = "http://localhost:1234";

  constructor(private http:Http) { 
  }

  public listarLibros():Observable<Libro[]>{
    return this.http.get(this.url+"/libros").
      map(response => response.json());
  }

  public buscarLibro(id:number):Observable<Libro>{
    return this.http.get(this.url+"/libros/"+id).
      map(response => response.json());
  }

}
