let express = require("express");
let bodyParser = require("body-parser");
let negocioLibros = require("./NegocioLibros.js"); 

let app = express();
app.listen(1234, function(){
    console.log("Esperando peticiones...");
});
// app.use(express.static('06_recursos_estaticos'));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
  });
app.disable('x-powered-by');

//get    /libros
//get    /libros/:id
//post   /libros
//put    /libros
//delete /libros/:id

app.get("/libros", function(request, response){
    let libros = negocioLibros.listarLibros();    
    response.json(libros);
});

app.get("/libros/:id", function(request, response){
    let id = request.params.id;
    let libro = negocioLibros.buscarLibro(id);
    if( libro == undefined ){
        response.sendStatus(404);
    } else {
        response.json(libro);
    }
});

app.post("/libros", function(request, response){
    let libro = request.body;
    negocioLibros.insertarLibro(libro);
    response.sendStatus(200);
});

app.put("/libros", function(request, response){
    let libro = request.body;
    negocioLibros.modificarLibro(libro);
    response.sendStatus(200);
});

app.delete("/libros/:id", function(request, response){
    let id = request.params.id;
    negocioLibros.borrarLibro(id);
    response.sendStatus(200);
});

