function Libro(id, titulo, autor, ISBN, precio) {
    this.id = id;
    this.titulo = titulo;
    this.autor = autor;
    this.ISBN = ISBN;
    this.precio = precio;
}

let libros = [];

libros.push(new Libro(1, "El Quijote", "Cervantes", "1234", 15));
libros.push(
    new Libro(2, "EL corazón de las tinieblas", "J.Conrad", "3210", 16)
);
libros.push(new Libro(3, "Tiempo anclado", "Julian Escobar", "7894", 17));
let contador = 3;

exports.insertarLibro = function(libro) {
    contador++;
    let libroAux = new Libro(
        contador,
        libro.titulo,
        libro.autor,
        libro.ISBN,
        libro.precio
    );
    libros.push(libroAux);
};

exports.listarLibros = function() {
    let librosAux = [];
    for (let a = 0; a < libros.length; a++) {
        let libro = new Libro(
            libros[a].id,
            libros[a].titulo,
            libros[a].autor,
            libros[a].ISBN,
            libros[a].precio
        );
        librosAux.push(libro);
    }
    return librosAux;
};

exports.buscarLibro = function(id) {
    for (let l of libros) {
        if (l.id == id) {
            return new Libro(l.id, l.titulo, l.autor, l.ISBN, l.precio);
        }
    }
    return null;
};

exports.modificarLibro = function(libro) {
    for (let l of libros) {
        if (l.id == libro.id) {
            l.titulo = libro.titulo;
            l.autor = libro.autor;
            l.ISBN = libro.ISBN;
            l.precio = libro.precio;
            break;
        }
    }
};

exports.borrarLibro = function(id) {
    for (let a = 0; a < libros.length; a++) {
        if (id == libros[a].id) {
            libros.splice(a, 1);
        }
    }
};
