import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { Padre1Component } from './componentes/padre1/padre1.component';
import { Padre2Component } from './componentes/padre2/padre2.component';
import { Hijo1Component } from './componentes/hijo1/hijo1.component';
import { Hijo2Component } from './componentes/hijo2/hijo2.component';
import { Hijo1Service } from 'app/servicios/hijo1.service';
import { GlobalService } from 'app/servicios/global.service';

@NgModule({
  declarations: [
    AppComponent,
    Padre1Component,
    Padre2Component,
    Hijo1Component,
    Hijo2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      // reglas de navegacion
      {
        'path' : 'padre1', // esto es arbitrario, es el nombre que quiera
        'component' : Padre1Component
      },
      {
        'path' : 'padre2', // esto es arbitrario, es el nombre que quiera
        'component' : Padre2Component
      }
    ])
  ],
  providers: [GlobalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
