import { Component, OnInit } from '@angular/core';
import { Padre2Service } from 'app/servicios/padre2.service';
import { GlobalService } from 'app/servicios/global.service';

@Component({
  selector: 'app-padre2',
  templateUrl: './padre2.component.html',
  styleUrls: ['./padre2.component.css'],
  providers : [Padre2Service]
})
export class Padre2Component implements OnInit {

  constructor(
    private globalService: GlobalService,
    private padre2Service: Padre2Service
    ) { 
    console.log('Padre2Component:constructor');
  }

  ngOnInit() {
  }

}
