import { Component, OnInit } from '@angular/core';
import { Hijo1Service } from 'app/servicios/hijo1.service';
import { GlobalService } from 'app/servicios/global.service';
import { Padre1Service } from 'app/servicios/padre1.service';

@Component({
  selector: 'app-hijo1',
  templateUrl: './hijo1.component.html',
  styleUrls: ['./hijo1.component.css'],
  providers: [Hijo1Service]
})
export class Hijo1Component implements OnInit {

  constructor(
    private hijo1Service : Hijo1Service,
    private padre1Service:Padre1Service,    
    private globalService: GlobalService) {
    console.log('Hijo1Component:constructor');
   }

  ngOnInit() {
  }

}
