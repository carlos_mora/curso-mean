import { Component, OnInit } from "@angular/core";
import { Hijo2Service } from "app/servicios/hijo2.service";
import { GlobalService } from "app/servicios/global.service";
import { Padre2Service } from "app/servicios/padre2.service";

@Component({
  selector: "app-hijo2",
  templateUrl: "./hijo2.component.html",
  styleUrls: ["./hijo2.component.css"],
  providers: [Hijo2Service, Padre2Service]
})
export class Hijo2Component implements OnInit {
  constructor(
    private globalService: GlobalService,
    private padre2Service: Padre2Service,
    private hijo2Service: Hijo2Service
  ) {
    console.log("Hijo2Component:constructor");
  }

  ngOnInit() {}
}
