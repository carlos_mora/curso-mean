import { Component, OnInit } from '@angular/core';
import { Padre1Service } from 'app/servicios/padre1.service';
import { GlobalService } from 'app/servicios/global.service';

@Component({
  selector: 'app-padre1',
  templateUrl: './padre1.component.html',
  styleUrls: ['./padre1.component.css'],
  providers: [Padre1Service]
})
export class Padre1Component implements OnInit {

  constructor(private globalService: GlobalService,
    private padre1Service:Padre1Service) {
    console.log('Padre1Component:constructor');
   }

  ngOnInit() {
  }

}
