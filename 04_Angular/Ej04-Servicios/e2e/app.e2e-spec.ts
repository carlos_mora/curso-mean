import { Ej04ServiciosPage } from './app.po';

describe('ej04-servicios App', function() {
  let page: Ej04ServiciosPage;

  beforeEach(() => {
    page = new Ej04ServiciosPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
