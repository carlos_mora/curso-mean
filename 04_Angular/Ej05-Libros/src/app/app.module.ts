import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LibrosFormularioComponent } from './componentes/libros-formulario/libros-formulario.component';
import { LibrosListadoComponent } from './componentes/libros-listado/libros-listado.component';
import { LibrosService } from 'app/servicios/libros.service';

@NgModule({
  declarations: [
    AppComponent,
    LibrosFormularioComponent,
    LibrosListadoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, // sirve para hacer peticiones
    RouterModule.forRoot([
      // reglas de navegacion
      {
        'path' : 'listado', // esto es arbitrario, es el nombre que quiera
        'component' : LibrosListadoComponent
      },
      /*{
        'path' : '', // esto es arbitrario, es el nombre que quiera
        'component' : Pagina1Component
      },*/
      {
        'path' : 'formulario',
        'component' : LibrosFormularioComponent
      },
      {
        'path' : 'formulario/:id',
        'component' : LibrosFormularioComponent,

      },
      {
        'path' : '**', // ruta por defecto
        'component' : LibrosListadoComponent
      }
    ])
  ],
  providers: [LibrosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
