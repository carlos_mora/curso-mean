import { Component, OnInit } from "@angular/core";
import { Libro } from "app/entidades/app.entidades.libro";
import { LibrosService } from "app/servicios/libros.service";

@Component({
  selector: "app-libros-listado",
  templateUrl: "./libros-listado.component.html",
  styleUrls: ["./libros-listado.component.css"]
})
export class LibrosListadoComponent implements OnInit {
  public libros: Libro[];
  constructor(private servicioLibros: LibrosService) {
    this.refrescarTabla();
  }
  
  ngOnInit() {}
  
  public refrescarTabla():void{
    this.libros = this.servicioLibros.listarLibros();
    
  }

}
