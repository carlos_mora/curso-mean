import { Component, OnInit } from "@angular/core";
import { Libro } from "app/entidades/app.entidades.libro";
import { Router, ActivatedRoute } from "@angular/router";
import { LibrosService } from "app/servicios/libros.service";

@Component({
  selector: "app-libros-formulario",
  templateUrl: "./libros-formulario.component.html",
  styleUrls: ["./libros-formulario.component.css"]
})
export class LibrosFormularioComponent implements OnInit {
  public libroSel: Libro;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private servicioLibros: LibrosService
  ) {
    let id:number = activatedRoute.snapshot.params["id"];
    if (id == undefined) {
      this.libroSel = new Libro(null, null, null, null, null);
    } else {
      this.libroSel = servicioLibros.buscarLibro(id);
    }
    // console.log(id);
  }

  public insertarLibro() {
    this.servicioLibros.insertarLibro(this.libroSel);
    this.irAlListado();
  }

  public modificarLibro() {
    this.servicioLibros.modificarLibro(this.libroSel);
    this.irAlListado();
  }
  public borrarLibro() {
    this.servicioLibros.borrarLibro(this.libroSel);
    this.irAlListado();
  }

  public irAlListado() {
    this.router.navigate(["/listado"]);
  }

  ngOnInit() {}
}
