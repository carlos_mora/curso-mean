import { Ej05LibrosPage } from './app.po';

describe('ej05-libros App', function() {
  let page: Ej05LibrosPage;

  beforeEach(() => {
    page = new Ej05LibrosPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
