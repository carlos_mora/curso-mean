import { Injectable } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';
import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class LibrosService {
    private url = 'http://localhost:1234';
    constructor(private http: Http) {}

    public insertarLibro(libro: Libro): Observable<Response> {
        const headers: Headers = new Headers({
            'content-type': 'application/json'
        });

        return this.http.post(
            this.url + '/libros',
            JSON.stringify(libro),
            { headers: headers }
        );
    }

    public listarLibros(): Observable<Libro[]> {
        return this.http
            .get(this.url + '/libros')
            .map(respuesta => respuesta.json());
    }

    public buscarLibro(id: number): Observable<Libro> {
        return this.http
            .get(this.url + '/libros/' + id)
            .map(respuesta => respuesta.json());
    }

    public modificarLibro(libro): Observable<Response> {
        const headers: Headers = new Headers({
            'content-type': 'application/json'
        });

        return this.http.put(
            this.url + '/libros',
            JSON.stringify(libro),
            { headers: headers }
        );
    }

    public borrarLibro(libro): Observable<Response> {
        return this.http.delete(
            this.url + '/libros/' + libro.id
        );
    }
}
