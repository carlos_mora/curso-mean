import { Component, OnInit } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';
import { Router, ActivatedRoute } from '@angular/router';
import { LibrosService } from 'app/servicios/libros.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Component({
    selector: 'app-libros-formulario',
    templateUrl: './libros-formulario.component.html',
    styleUrls: ['./libros-formulario.component.css']
})
export class LibrosFormularioComponent implements OnInit {
    public libroSel: Libro;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private servicioLibros: LibrosService
    ) {
        const id: number = activatedRoute.snapshot.params['id'];
        this.libroSel = new Libro(null, null, null, null, null);
        if (id !== undefined) {
            servicioLibros.buscarLibro(id).subscribe(
                data => this.libroSel = data,
                error => console.log('error ' + error)
            );
        }
        // console.log(id);
    }

    public insertarLibro() {
        this.servicioLibros.insertarLibro(this.libroSel)
        .subscribe(
            () => this.irAlListado(),
            error => console.log('error ' + error)
        );
    }

    public modificarLibro() {
        this.servicioLibros.modificarLibro(this.libroSel)
        .subscribe(
            () => this.irAlListado(),
            error => console.log('error ' + error)
        );
    }
    public borrarLibro() {
        this.servicioLibros.borrarLibro(this.libroSel)
        .subscribe(
            () => this.irAlListado(),
            error => console.log('error ' + error)
        );
    }

    public irAlListado() {
        this.router.navigate(['/listado']);
    }

    ngOnInit() {}
}
