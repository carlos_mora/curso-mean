import { Component, OnInit } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';
import { LibrosService } from 'app/servicios/libros.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Component({
    selector: 'app-libros-listado',
    templateUrl: './libros-listado.component.html',
    styleUrls: ['./libros-listado.component.css']
})
export class LibrosListadoComponent implements OnInit {
    public libros: Libro[];
    constructor(private servicioLibros: LibrosService, private http: Http) {
        this.refrescarTabla();
    }

    ngOnInit() {}

    public refrescarTabla(): void {
        this.servicioLibros
        .listarLibros()
        .subscribe(
                data => this.libros = data,
                error => console.log('catapúm'),
                () => console.log('esto va siempre (finally)')
            );
    }
}
