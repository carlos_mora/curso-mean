import { Ej06HttpPage } from './app.po';

describe('ej06-http App', function() {
  let page: Ej06HttpPage;

  beforeEach(() => {
    page = new Ej06HttpPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
