const mongoose   = require("mongoose");
const Usuario    = require("./usuario.js").Usuario;
const Cliente    = require("./cliente.js").Cliente;
const Empleado   = require("./empleado.js").Empleado;
const Incidencia = require("./incidencia.js").Incidencia;

const url = "mongodb://localhost:27017/incidencias"
mongoose.Promise = global.Promise;
mongoose.connect(url, { useMongoClient:true });

let usr = {
    nombre : 'Terence Hill',
    login  : 't',
    pw     : 't',
    idioma : 'ES'
}
usr.tipo = 'cliente';

let cli = {
    _id : null,
    nombre : usr.nombre,
    cuentaBancaria : null
}


let incidencia = {
    asunto          : 'Han desaparecido los datos',
    descripcion     : 'bla bla blá',
    fechaAlta       : undefined, 
    fechaLimite     : '01/01/2085',
  /*fechaResolucion : Date,*/
    prioridad       : 'Baja',
    estado          : 'Creada',
    cliente         : cli    
}

function movidon(){
    let usuarioMG = new Usuario(usr);
    
    return usuarioMG.save().
    then(function (usuarioMG){
        let clienteMG = new Cliente(cli);
        clienteMG._id = usuarioMG._id;
        return clienteMG.save();
    }).
    then(function(clienteMG){
        let incidenciaMG = new Incidencia(incidencia);
        incidenciaMG.cliente = clienteMG;
        return incidenciaMG.save();
    }).
    then(function(incidenciaMG){
        mongoose.disconnect();
        return "ok";
    });
}








