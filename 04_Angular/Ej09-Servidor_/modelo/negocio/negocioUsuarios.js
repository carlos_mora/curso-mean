const Usuario    = require("../entidades/usuario.js").Usuario; // :(
const Cliente    = require("../entidades/cliente.js").Cliente; // :(
const Empleado   = require("../entidades/empleado.js").Empleado; // :(
const Incidencia = require("../entidades/incidencia.js").Incidencia; // :(
//const Usuario = require("/modelo/entidades/usuario.js").Usuario; // :) 

exports.buscarPorLogin = function(login, pw){
    return Usuario.findOne({ login:login, pw:pw }); //La promesa de quizás un usuario
}

exports.modificar = function( usuario ){

    switch( usuario.tipo){
        case 'cliente'  : return modificarCliente(usuario);
        case 'empleado' : return modificarEmpleado(usuario);
    }

}

function modificarCliente(usuario){

    return Usuario.findByIdAndUpdate(usuario._id, 
                                     usuario, 
                                     { new:true } ).
    then( usuarioMG => {
       let datos = { 'nombre' : usuarioMG.nombre };
       return Cliente.findByIdAndUpdate( usuarioMG._id, 
                                         datos, 
                                         { new:true } );
    }).
    then( clienteMG => {
        return Incidencia.update( { 'cliente._id' : clienteMG._id },
                                  { 'cliente' : clienteMG },
                                  { 'multi' : true } );
    }).
    then( XX => {
        return true;
    });

}

function modificarEmpleado(usuario){

    return Usuario.findByIdAndUpdate(usuario._id, 
                                     usuario, 
                                     { new:true } ).
    then( usuarioMG => {
    let datos = { 'nombre' : usuarioMG.nombre };
    return Empleado.findByIdAndUpdate( usuarioMG._id, 
                                       datos, 
                                       { new:true } );
    }).
    then( empleadoMG => {
    return Incidencia.update( { 'empleado._id' : empleadoMG._id },
                              { 'empleado' : empleadoMG },
                              { 'multi' : true } );
    }).
    then( XX => {
        return true;
    });

}



