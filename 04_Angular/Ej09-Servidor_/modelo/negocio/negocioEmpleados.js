const Usuario = require("../entidades/usuario.js").Usuario; 
const Empleado = require("../entidades/empleado.js").Empleado; 
const Incidencia = require("../entidades/incidencia.js").Incidencia; 

//nombre
//login
//pw
//idioma
//sueldo
//codEmp
//dpto
exports.insertar = function(datosEmpleado){

    let usuarioMG = new Usuario(datosEmpleado);
    
    return usuarioMG.save().
    then( usuarioMG => {
        let empleadoMG = new Empleado(datosEmpleado);
        empleadoMG._id = usuarioMG._id;

        return empleadoMG.save()
    }).
    then( empleadoMG => {
        return true;
    });

}

exports.listarCargaTrabajo = function(){

    //buscar empleados que no tengan incidencias

    return Incidencia.aggregate([
        //{
        //    $match: { estado : 'Alta'}
        //},
        {
            $group: { _id   : "$empleado._id",
                      count : {$sum : 1} }
        },
        {   $sort : {count:1} } 
        ]);

}

exports.modificar = function(usuario){
}

exports.borrar = function(_id){    
}

exports.buscar = function(_id){
}

exports.listar = function(){
}
