const express = require("express");
const mongooseUtil = require("./util/mongooseUtil.js");
const bodyParser = require("body-parser");
const usuariosRest = require("./rest_apis/usuariosRest.js");
const incidenciasRest = require("./rest_apis/incidenciasRest.js");

const app = express();

let promesa = mongooseUtil.conectarBBDD();
promesa.then(function() {
    app.listen(4321, function() {
        console.log("Esperando peticiones...");
    });
}).
catch(function() {
    console.log("No se pudo conectar a la bb.dd.");
    console.log("Adios mundo cruel");
    process.exit(1);
});

app.use(bodyParser.json());
app.disable('x-powered-by');
//Para el cross origin:
app.use(function(request, response, next) {
    console.log("Filtro de Origin");
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    response.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    next(); //tira, arrea palante
});

//
app.use("/", usuariosRest.router);
app.use("/", incidenciasRest.router);
//