//npm install basic-auth
const auth = require("basic-auth");
const Usuario = require("../modelo/entidades/usuario.js").Usuario;
const negocioUsuarios = require("../modelo/negocio/negocioUsuarios.js");

exports.comprobarCredenciales = function(request, response, callback){

    let credenciales = auth(request);
    console.log("=========================================");
    console.log(credenciales);

    if(!credenciales){
        response.setHeader('WWW-Authenticate', 'Basic realm=ejemplo');
        response.sendStatus(401);
        return;
    }  
    
    let criterio = { 'login' : credenciales.name,
                     'pw'    : credenciales.pass };    
    
    negocioUsuarios.buscarPorLogin( credenciales.name, 
                                    credenciales.pass).
    then( usuarioMG => {
        if(!usuarioMG){
            response.setHeader('WWW-Authenticate', 'Basic realm=ejemplo');
            response.sendStatus(401);
            return;
        }
        //El usuario existe!
        callback(usuarioMG);
        //
    });

};





