import { Ej03NavegacionPage } from './app.po';

describe('ej03-navegacion App', function() {
  let page: Ej03NavegacionPage;

  beforeEach(() => {
    page = new Ej03NavegacionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
