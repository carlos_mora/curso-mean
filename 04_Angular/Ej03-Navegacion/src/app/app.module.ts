import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { Pagina1Component } from './componentes/pagina1/pagina1.component';
import { Pagina2Component } from './componentes/pagina2/pagina2.component';

@NgModule({
  declarations: [
    AppComponent,
    Pagina1Component,
    Pagina2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, // sirve para hacer peticiones
    RouterModule.forRoot([
      // reglas de navegacion
      {
        'path' : 'pagina1/:dato', // esto es arbitrario, es el nombre que quiera
        'component' : Pagina1Component
      },
      /*{
        'path' : '', // esto es arbitrario, es el nombre que quiera
        'component' : Pagina1Component
      },*/
      {
        'path' : 'pagina2/:info/:param',
        'component' : Pagina2Component
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
