import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pagina2',
  templateUrl: './pagina2.component.html',
  styleUrls: ['./pagina2.component.css']
})
export class Pagina2Component implements OnInit {
  public info:string;
  public param:string;
  constructor(private router:Router, activatedRoute: ActivatedRoute) { 
    console.log('Pagina2Component: constructor');
    // console.log(activatedRoute);
    // activatedRoute representa la regla de navegacion que ha traido hasta el componente
    this.info = activatedRoute.snapshot.params['info'];
    this.param = activatedRoute.snapshot.params['param'];
    console.log(this.info);
  }

  ngOnInit() {
  }

  public navegacionProgramatica():void {
    this.router.navigate(['pagina1', 2222 ])
  }


}
