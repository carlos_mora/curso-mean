import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pagina1',
  templateUrl: './pagina1.component.html',
  styleUrls: ['./pagina1.component.css']
})
export class Pagina1Component implements OnInit {
  public dato:string;
  // inyeccion de dependencias
  // el constructor se ponen cosas que te van a dar, vienen por inyeccion, son dependencias
  constructor(private router:Router, private activatedRoute: ActivatedRoute, ) { 
    console.log('Pagina1Component: constructor');
    // console.log(activatedRoute);
    // activatedRoute representa la regla de navegacion que ha traido hasta el componente
    this.dato = activatedRoute.snapshot.params['dato'];
    console.log(this.dato);
  }

  ngOnInit() {
  }
  public navegacionProgramatica():void {
    this.router.navigate(['pagina2', 333, 444 ])
  }
}
