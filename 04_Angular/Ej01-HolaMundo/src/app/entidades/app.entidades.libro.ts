// Nombres de las clases en UppeCamelCase
export class Libro {
  // no usamos privates ni accesores
  constructor(
    public titulo: string,
    public autor: string,
    public ISBN: string,
    public precio: number,
  ) {

  }

  // toString
  public toString():string {
    return this.titulo+', '
    +this.autor+', '
    +this.ISBN+', '
    +this.precio;
  }
}
