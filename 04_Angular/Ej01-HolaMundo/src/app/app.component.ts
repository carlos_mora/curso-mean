import { Component } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // da igual que sea privado ya que se transpila a JS
  private numero:number = 6.32;
  public title:string = 'Mola Hundo';
  public mensaje:string = 'que bien...';
  public libro: Libro;

  private contador:number = 0;

  constructor(){
    this.libro = new Libro('La catedral del mar', 'Ildefonso Falcones', '91270908324790', 23.90);
  }

  public btnDale():void{
    this.libro.precio = this.libro.precio * 2;
    console.log('Le doy...');
    this.contador++;
  }

  public btnSiguiente():void{
    console.log('Se ha pulsado '+this.contador+'veces');
  }
}
