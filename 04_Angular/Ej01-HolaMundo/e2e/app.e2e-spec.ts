import { Ej01HolaMundoPage } from './app.po';

describe('ej01-hola-mundo App', function() {
  let page: Ej01HolaMundoPage;

  beforeEach(() => {
    page = new Ej01HolaMundoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
