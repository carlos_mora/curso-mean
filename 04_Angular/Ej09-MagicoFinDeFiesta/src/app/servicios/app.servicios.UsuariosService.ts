import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Usuario } from "app/entidades/entidades.usuario";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { SeguridadUtil } from "app/util/app.util.seguridadUtil";

@Injectable()
export class UsuariosService{

    private url:string = "http://localhost:4321";

    constructor(private http:Http,
                private seguridadUtil:SeguridadUtil){
    }

    public buscarPorLogin(usuario:Usuario):Observable<Usuario>{

        //Aqui seguridadUtil no nos sirve: no tenemos usuario en 'sesionService'
        let token:String = "Basic " + btoa( usuario.login+":"+usuario.pw );
        let headers:Headers= 
            new Headers( { 'Authorization': token } );

        let parametros:string="?login="+usuario.login+"&pw="+usuario.pw;
        
        return this.http.get(this.url+"/usuarios/credenciales"+parametros, 
                             {  'headers' : headers })
            .map(response => response.json());

    }

    public logout():Observable<Response>{
        return this.http.get(this.url+"/logout");        
    }

    public insertar(usuario:Usuario):Observable<Response>{
        //Esta petición es pública y no tiene token login:pw
        let headers:Headers= new Headers( { 'Content-Type':'Application/json' } );
        return this.http.post(this.url+"/clientes", 
                         JSON.stringify(usuario), 
                         { 'headers':headers } );
    }
                            
    public modificar(usuario:Usuario):Observable<Response>{
        return this.http.put(this.url+"/usuarios", 
                         JSON.stringify(usuario), 
                         { 'headers':this.seguridadUtil.headerAuthJson } );        
    }

    public borrar(usuario:Usuario):void{
        //
    }

}