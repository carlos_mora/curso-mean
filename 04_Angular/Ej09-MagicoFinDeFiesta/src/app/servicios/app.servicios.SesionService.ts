
import { Injectable } from "@angular/core";

@Injectable()
export class SesionService{

    private sesion = [];

    constructor(){
    }

    public add(objeto:any, clave:string):void{
        this.sesion[clave] = objeto;
    }

    public get(clave:string):any{
        return this.sesion[clave];
    }  

    public remove(clave:string):void{
        delete this.sesion[clave];
    }

}



