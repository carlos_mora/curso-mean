import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Usuario } from "app/entidades/entidades.usuario";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Incidencia } from "app/entidades/entidades.incidencia";
import { SeguridadUtil } from "app/util/app.util.seguridadUtil";

@Injectable()
export class IncidenciasService{

    private url:string = "http://localhost:4321";
    
    constructor(private http:Http,
                private seguridadUtil:SeguridadUtil){
    }

    public buscar(_id:String):Observable<Incidencia>{
        return this.http.get(this.url+"/incidencias/"+_id,
                             { 'headers':this.seguridadUtil.headerAuth } ).
                map( res => res.json()); 
    }

    public listarPorCliente(_id:String):Observable<Incidencia[]>{
        return this.http.get(this.url+"/incidencias/clientes/"+_id,
                             { 'headers':this.seguridadUtil.headerAuth } ).
                map( res => res.json()); 
    }

    public listarPorEmpleado(_id:String):Observable<Incidencia[]>{
        return this.http.get(this.url+"/incidencias/empleados/"+_id,
                             { 'headers':this.seguridadUtil.headerAuth }).
                map( res => res.json()); 
    }

    public insertar(incidencia:Incidencia):Observable<Response>{
        return this.http.post(this.url+"/incidencias", 
                              JSON.stringify(incidencia),
                              { 'headers':this.seguridadUtil.headerAuthJson });
    }

    public modificar(incidencia:Incidencia):Observable<Response>{
        return this.http.put(this.url+"/incidencias", 
                             JSON.stringify(incidencia),
                             { 'headers':this.seguridadUtil.headerAuthJson });
    }

}
