import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';

import { LoginComponent } from 'app/componentes/login/login.component';
import { CuentaComponent } from 'app/componentes/cuenta/cuenta.component';
import { AltaUsuarioComponent } from 'app/componentes/alta-usuario/alta-usuario.component';
import { AceptacionCondicionesComponent } from 'app/componentes/aceptacion-condiciones/aceptacion-condiciones.component';
import { AppClienteComponent } from 'app/componentes/app-cliente/app-cliente.component';
import { AppEmpleadoComponent } from 'app/componentes/app-empleado/app-empleado.component';
import { InicioClienteComponent } from 'app/componentes/inicio-cliente/inicio-cliente.component';
import { FormIncidenciaCliComponent } from 'app/componentes/form-incidencia-cli/form-incidencia-cli.component';
import { InicioEmpleadoComponent } from 'app/componentes/inicio-empleado/inicio-empleado.component';
import { FormIncidenciaEmpComponent } from "app/componentes/form-incidencia-emp/form-incidencia-emp.component";

//Rutas para el router-outlet presente en AplicacionComponent
const rutasAppCliente:Routes = [
  { 'path'      : 'inicio',
    'component' : InicioClienteComponent },
  { 'path'      : 'formIncidencia',
    'component' : FormIncidenciaCliComponent },
  { 'path'      : 'cuenta',
    'component' : CuentaComponent }
 ];

 const rutasAppEmpleado:Routes = [
  { 'path'      : 'inicio',
    'component' : InicioEmpleadoComponent },
  { 'path'      : 'formIncidencia/:id',
    'component' : FormIncidenciaEmpComponent },
  { 'path'      : 'cuenta',
    'component' : CuentaComponent }
 ];

//Rutas para el router-outlet presente en AppComponent
const rutas:Routes = [
  { 'path'      : '',
    'component' : LoginComponent },

  { 'path'      : 'login',
    'component' : LoginComponent },

  { 'path'      : 'alta',
    'component' : AltaUsuarioComponent },

  { 'path'      : 'aceptacion',
    'component' : AceptacionCondicionesComponent },

  { 'path'      : 'appCliente',
    'component' : AppClienteComponent,
    'children'  : rutasAppCliente },

  { 'path'      : 'appEmpleado',
    'component' : AppEmpleadoComponent,
    'children'  : rutasAppEmpleado }    
];

@NgModule({
  imports: [ RouterModule.forRoot(rutas) ],
  exports: [ RouterModule ]
})
export class AppRutasModule { }
