
export class Incidencia {

    public _id      : string;
    public asunto   : string;
    public descripcion     : string;
    public fechaAlta       : Date;
    public fechaLimite     : Date;
    public fechaResolucion : Date;
    public prioridad       : string;
    public estado          : string;
    public cliente         : any;
    public empleado        : any;    

    constructor(){
    }

    //setDatos({})

}
