import { Component, OnInit } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { Router } from '@angular/router';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { SeguridadUtil } from "app/util/app.util.seguridadUtil";

@Component({
  selector    : 'app-login',
  templateUrl : './login.component.html',
  styleUrls   : ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public usuario:Usuario;
  public mensaje:string = "";

  constructor(private router:Router,
              private sesionService:SesionService,
              private usuariosService:UsuariosService,
              private seguridadUtil:SeguridadUtil) { 

    //Si el usuario está autenticado hacemos logout!
    if(sesionService.get("usuario")){
      this.usuariosService.logout().
            subscribe( null,
                      err => {
                          console.log( err );
                          this.sesionService.remove("usuario");
                          this.router.navigate( ['/login'] );
                      });
    }
                    
    this.usuario = new Usuario(null,null,'f','f',null,null);


  }

  ngOnInit() {
  }

  public login(){
    
    this.usuariosService.buscarPorLogin(this.usuario).
      subscribe(usr => {
                  this.sesionService.add(usr, "usuario");

                  //Invocamos 'crearHeaders' de 'seguridadUtil' ahora que 
                  //sabemos cuál es el usuario y este está en 'sesionService'
                  this.seguridadUtil.crearHeaders();

                  switch(usr.tipo){
                    case "cliente"  :  this.router.navigate( ['/appCliente/inicio'] );
                                       break;
                    case "empleado" :  this.router.navigate( ['/appEmpleado/inicio'] );
                                       break;
                    default : console.log("Atiza!");
                  }

                },
                error=> {
                  this.mensaje = "Credenciales incorrectas";
                });
  }

}





