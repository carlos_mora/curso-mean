import { Component, OnInit } from '@angular/core';
import { Incidencia } from 'app/entidades/entidades.incidencia';
import { IncidenciasService } from 'app/servicios/app.servicios.IncidenciasService';
import { SesionService } from 'app/servicios/app.servicios.SesionService';

@Component({
  selector: 'app-inicio-cliente',
  templateUrl: './inicio-cliente.component.html',
  styleUrls: ['./inicio-cliente.component.css']
})
export class InicioClienteComponent implements OnInit {

  public incidencias:Incidencia[];

  constructor(private incidenciasService : IncidenciasService,
              private sesionService      : SesionService) { 
    this.obtenerIncidencias();
  }

  ngOnInit() {
  }

  public obtenerIncidencias():void{
    let _id:String = this.sesionService.get("usuario")._id;

    this.incidenciasService.listarPorCliente(_id).
      subscribe( incidencias => this.incidencias = incidencias ,
                 err => console.log(err) );
  }

}
