import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Router } from '@angular/router';
import { UsuariosService } from "app/servicios/app.servicios.UsuariosService";

@Component({
  selector: 'appEmpleado',
  templateUrl: './app-empleado.component.html',
  styleUrls: ['./app-empleado.component.css']
})

export class AppEmpleadoComponent  implements OnInit {

  public usuario:Usuario;

  constructor(private sesionService:SesionService,
              private usuariosService:UsuariosService,
              private router:Router) { 
    this.usuario = sesionService.get("usuario");
  }

  ngOnInit() {
  }

  /*Esto está mejor en LoginComponent. Asi no se repite! 
  public logout(){
    this.usuariosService.logout().
          subscribe( null,
                     err => {
                        console.log( err );
                        this.sesionService.remove("usuario");
                        this.router.navigate( ['/login'] );
                     });
  }
  */

}
