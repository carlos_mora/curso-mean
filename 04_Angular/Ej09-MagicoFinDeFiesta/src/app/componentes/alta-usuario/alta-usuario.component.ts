import { Component, OnInit } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Router } from '@angular/router';
import { SesionService } from 'app/servicios/app.servicios.SesionService';

@Component({
  selector: 'app-alta-usuario',
  templateUrl: './alta-usuario.component.html',
  styleUrls: ['./alta-usuario.component.css']
})
export class AltaUsuarioComponent implements OnInit {

  public usuario:Usuario;
  public confirmacionPW:string;
  public mensaje:string;

  constructor(private sesionService:SesionService,
              private router:Router) { 
    this.usuario = new Usuario(undefined,null,null,null,null, null); 
  }

  ngOnInit() {
  }

  public siguiente():void{
    if(this.usuario.pw != this.confirmacionPW){
      this.mensaje = "Pw y confirmación pw no coinciden";
      return;
    } 

    this.sesionService.add(this.usuario, "usuario");
    this.router.navigate(['aceptacion']);
  }

}

