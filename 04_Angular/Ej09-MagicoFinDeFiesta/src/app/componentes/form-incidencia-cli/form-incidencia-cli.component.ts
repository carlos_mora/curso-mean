import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { IncidenciasService } from 'app/servicios/app.servicios.IncidenciasService';
import { Incidencia } from 'app/entidades/entidades.incidencia';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-incidencia-cli',
  templateUrl: './form-incidencia-cli.component.html',
  styleUrls: ['./form-incidencia-cli.component.css']
})
export class FormIncidenciaCliComponent implements OnInit {

  public incidencia:Incidencia;

  constructor(private sesionService:SesionService,
              private incidenciasService:IncidenciasService,
              private router:Router ) { 
    this.incidencia = new Incidencia();
  }

  ngOnInit() {
  }

  public insertar():void{

    let _id = this.sesionService.get("usuario")._id; 
    this.incidencia.cliente = { '_id' : _id };

    this.incidenciasService.insertar(this.incidencia).
      subscribe( res => { this.router.navigate(['/appCliente/inicio']) },
                 err => { console.log(err) } );

  }

  public modificar():void{
  }

  public borrar():void{
  }

  public vaciar():void{
  }

}
