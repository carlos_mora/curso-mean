import { Component, OnInit } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { Router } from "@angular/router";

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.css']
})
export class CuentaComponent implements OnInit {

  public usuario:Usuario;
  public avisoVisible:boolean=false;

  constructor(private sesionService:SesionService,
              private usuariosService:UsuariosService,
              private router:Router) {

    let usuario = sesionService.get("usuario");
    //Usamos un clon del objeto guardado en 'sesionService' para que no se
    //modifique hasta que se pulse 'Guardar'
    this.usuario = Object.assign(new Usuario(null,null,null,null,null,null), usuario);
    console.log(this.usuario);

  }

  ngOnInit() {
  }

  public mostrarAviso(){
    this.avisoVisible = true;
  }

  public guardar(){

    this.avisoVisible = false;

    this.usuariosService.modificar(this.usuario).
      subscribe( rs => {
                  console.log("Parece que ya...");
                  //Forzamos el logout para actualizar la aplicación
                  //this.sesionService.add(this.usuario, "usuario");                 
                  this.router.navigate( [ '/login' ] );
                 },
                 err => console.log(err) );
  }

}
