import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Router } from '@angular/router';
import { UsuariosService } from "app/servicios/app.servicios.UsuariosService";

@Component({
  selector: 'appCliente',
  templateUrl: './app-cliente.component.html',
  styleUrls: [ './app-cliente.component.css']
})

export class AppClienteComponent  implements OnInit {

  public usuario:Usuario;

  constructor(private sesionService:SesionService,
              private router:Router,
              private usuariosService:UsuariosService) { 
    this.usuario = sesionService.get("usuario");
  }

  ngOnInit() {
  }

  /*Esto está mejor en LoginComponent. Asi no se repite! 
  public logout(){
    this.usuariosService.logout().
          subscribe( null,
                     err => {
                        console.log( err );
                        this.sesionService.remove("usuario");
                        this.router.navigate( ['/login'] );
                     })
  }
  */

}
