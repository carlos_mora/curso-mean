import { Component, OnInit } from '@angular/core';
import { Incidencia } from 'app/entidades/entidades.incidencia';
import { Usuario } from 'app/entidades/entidades.usuario';
import { IncidenciasService } from 'app/servicios/app.servicios.IncidenciasService';
import { SesionService } from 'app/servicios/app.servicios.SesionService';

@Component({
  selector: 'app-inicio-empleado',
  templateUrl: './inicio-empleado.component.html',
  styleUrls: ['./inicio-empleado.component.css']
})
export class InicioEmpleadoComponent implements OnInit {

  public incidencias:Incidencia[];

  constructor(private incidenciasService:IncidenciasService,
              private sesionService:SesionService) { 
    this.obtenerIncidencias();
  }

  ngOnInit() {
  }

  public obtenerIncidencias(){
    let id = this.sesionService.get("usuario")._id;
    this.incidenciasService.listarPorEmpleado(id).
        subscribe( incidencias => this.incidencias = incidencias,
                   err => console.log(err) );
  }

}
