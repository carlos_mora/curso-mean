import { Component, OnInit } from '@angular/core';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { Router } from '@angular/router';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';

@Component({
  selector: 'app-aceptacion-condiciones',
  templateUrl: './aceptacion-condiciones.component.html',
  styleUrls: ['./aceptacion-condiciones.component.css']
})
export class AceptacionCondicionesComponent implements OnInit {

  public  checkAcepto:boolean;
  public  mensaje:string;
  private usuario:Usuario;

  constructor(private router:Router,
              private usuariosService:UsuariosService,
              private sesionService:SesionService) { 
    this.usuario = sesionService.get("usuario");
  }

  ngOnInit() {
  }

  public fin():void{

    if( !this.checkAcepto ){
      this.mensaje = "Por favor marque 'Acepto'";
      return;
    }

    this.usuariosService.insertar(this.usuario).
      subscribe( response => this.recargarUsuario(),
                 error => console.log("ZASCA al insertar"));
  }

  private recargarUsuario(){
    this.usuariosService.buscarPorLogin(this.usuario).
      subscribe(usr => {
        this.sesionService.add(usr,"usuario");
        this.router.navigate( [ '/appCliente' ]);
      },
      error => console.log("ATPC"));
  }

}
