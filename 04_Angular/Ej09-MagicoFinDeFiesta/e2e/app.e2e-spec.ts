import { Ej09MagicoFindeFiestaPage } from './app.po';

describe('ej09-magico-finde-fiesta App', function() {
  let page: Ej09MagicoFindeFiestaPage;

  beforeEach(() => {
    page = new Ej09MagicoFindeFiestaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
