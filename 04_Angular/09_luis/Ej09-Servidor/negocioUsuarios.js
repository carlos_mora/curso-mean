const Usuario = require("./usuario.js").Usuario; 

exports.insertar = function(usuario){
    return new Usuario(usuario).save();
}

exports.modificar = function(usuario){
    return Usuario.findByIdAndUpdate(usuario._id, usuario);
}

exports.borrar = function(_id){
    
    /*Con callbacks
    //si lo hacemos con callbacks la funcion 'borrar' no devuelve nada
    Usuario.findById(_id, function(err, usr){
        if(err) { throw err; }
        usr.remove(function(err,usr){
            if(err) { throw err; }
            console.log("Eliminado");            
        });
    });
    */

    /*Con promesas
    //Es la manera en la que lafunción borrar puede devolver algo, aunque
    //sea la promesa de que el usuario se va a borrar...
    return Usuario.findById(_id).then(function(usr){
        return usr.remove();
    }).then(function(rs){
        //
    });
    */

    //Si sabemos que borramos elusuario si o si:
    return Usuario.findByIdAndRemove(_id);
}

exports.buscar = function(_id){

}

exports.listar = function(){}

exports.buscarPorLogin = function(login, pw){}
