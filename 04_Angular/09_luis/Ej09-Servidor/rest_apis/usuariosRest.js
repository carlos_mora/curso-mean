const express = require("express");
const negocioUsuarios  = require("../modelo/negocio/negocioUsuarios.js");
const negocioClientes  = require("../modelo/negocio/negocioClientes.js");
const negocioEmpleados = require("../modelo/negocio/negocioEmpleados.js");

let router = express.Router();

router.get("/usuarios/credenciales", function(request, response){
    let login = request.query.login;
    let pw = request.query.pw;

    console.log(login+","+pw);

    let promesa = negocioUsuarios.buscarPorLogin(login, pw);
    promesa.then( usuario => {
                        console.log("Buscar por login. Usuario:"+usuario);
                        if(usuario){
                            response.json(usuario); 
                        } else {
                            response.sendStatus(404);  
                        }
                    })
           .catch( err => response.sendStatus(500) );
});

router.post("/clientes", function(request, response){
    negocioClientes.insertar(request.body).
        then( ok => response.sendStatus(200)).
        catch( err => { console.log(err);
                        response.sendStatus(500) });
});

router.post("/empleados", function(request, response){
    negocioEmpleados.insertar(request.body).
        then( ok => response.sendStatus(200)).
        catch( err => response.sendStatus(500));
});

exports.router = router;

/*
use incidencias
db.usuarios.update( { nombre : 'Harry Callahan'},
                    { $set: { tipo : 'Empleado' } })
*/

