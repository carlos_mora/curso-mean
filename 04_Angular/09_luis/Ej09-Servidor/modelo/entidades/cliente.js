const mongoose = require("mongoose");

let esquemaCliente = new mongoose.Schema({
    _id            : mongoose.Schema.ObjectId,
    nombre         : String,
    cuentaBancaria : String
});

exports.Cliente = mongoose.model('clientes', esquemaCliente);


