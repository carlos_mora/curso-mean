const mongoose = require("mongoose");
const Cliente = require("./cliente.js").Cliente;
const Empleado = require("./empleado.js").Empleado;

let esquemaIncidencia = new mongoose.Schema({
    asunto          : String,
    descripcion     : String,
    fechaAlta       : { type: Date, default: Date.now }, 
    fechaLimite     : Date,
    fechaResolucion : Date,
    prioridad       : String,
    estado          : String,

    cliente  : Cliente.schema,
    empleado : Empleado.schema

});

exports.Incidencia = mongoose.model('incidencias', esquemaIncidencia);




