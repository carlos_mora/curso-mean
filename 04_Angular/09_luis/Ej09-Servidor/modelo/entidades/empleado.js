const mongoose = require("mongoose");

let esquemaEmpleado = new mongoose.Schema({
    _id            : mongoose.Schema.ObjectId,
    nombre         : String,
    sueldo         : Number,
    codigoEmpleado : String,
    departamento   : String
});

exports.Empleado = mongoose.model('empleados', esquemaEmpleado);
