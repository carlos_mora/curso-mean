import { Component, OnInit } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { Router } from '@angular/router';
import { SesionService } from 'app/servicios/app.servicios.SesionService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public usuario:Usuario;
  public mensaje:string = "";

  constructor(private router:Router,
              private sesionService:SesionService,
              private usuariosService:UsuariosService) { 
    this.usuario = new Usuario(null,null,null,null,null);
  }

  ngOnInit() {
  }

  public login(){
    
    this.usuariosService.buscarPorLogin(this.usuario).
      subscribe(data => {
                  this.sesionService.add(data, "usuario");
                  this.router.navigate( ['/aplicacion'] );
                },
                error=> {
                  this.mensaje = "Credenciales incorrectas";
                });
  }

}
