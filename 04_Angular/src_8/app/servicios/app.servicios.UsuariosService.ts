import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Usuario } from "app/entidades/entidades.usuario";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class UsuariosService{

    private url:string = "http://localhost:4321";

    constructor(private http:Http){
    }

    public buscarPorLogin(usuario:Usuario):Observable<Usuario>{
        let parametros:string="?login="+usuario.login+"&pw="+usuario.pw;
        return this.http.get(this.url+"/usuarios/credenciales"+parametros)
            .map(response => response.json());
    }

    public insertar(usuario:Usuario):void{
        //
    }
    
    public modificar(usuario:Usuario):void{
        //
    }

}