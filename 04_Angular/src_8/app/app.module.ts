import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRutasModule } from 'app/app.rutas.module';

import { AppComponent } from './app.component';
import { LoginComponent } from 'app/componentes/login/login.component';
import { InicioComponent } from 'app/componentes/inicio/inicio.component';
import { CuentaComponent } from 'app/componentes/cuenta/cuenta.component';
import { AplicacionComponent } from 'app/componentes/aplicacion/aplicacion.component';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { AltaUsuarioComponent } from './componentes/alta-usuario/alta-usuario.component';
import { AceptacionCondicionesComponent } from './componentes/aceptacion-condiciones/aceptacion-condiciones.component';

@NgModule({
  declarations: [
    LoginComponent,
    InicioComponent,
    CuentaComponent,
    AplicacionComponent,
    AppComponent,
    AltaUsuarioComponent,
    AceptacionCondicionesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRutasModule
  ],
  providers: [ SesionService,
               UsuariosService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
