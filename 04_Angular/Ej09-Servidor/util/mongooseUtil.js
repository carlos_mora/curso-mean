const mongoose = require("mongoose");
mongoose.Promise = global.Promise; //Cágate lorito
const url = "mongodb://localhost:27017/incidencias";

exports.conectarBBDD = function(){
    return mongoose.connect(url, { useMongoClient: true });
}

exports.desconectarBBDD = function(){
    return mongoose.disconnect();
}