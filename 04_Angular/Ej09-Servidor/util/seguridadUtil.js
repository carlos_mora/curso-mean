//npm install basic-auth
const auth = require("basic-auth");
const Usuario = require("../modelo/entidades/usuario.js").Usuario;
const negocioUsuarios = require("../modelo/negocio/negocioUsuarios.js");

//'tipo' quedaría mejor como 'rol'
exports.comprobarCredenciales = function(request, response, callback, tipo){

    let credenciales = auth(request);
    console.log("=========================================");
    console.log(credenciales);

    if(!credenciales){
        response.setHeader('WWW-Authenticate', 'Basic realm=ejemplo');
        response.sendStatus(401);
        return;
    }  
    
    let criterio = { 'login' : credenciales.name,
                     'pw'    : credenciales.pass };    
    
    negocioUsuarios.buscarPorLogin( credenciales.name, 
                                    credenciales.pass).
    then( usuarioMG => {
        //El usuario existe
        if(!usuarioMG){
            response.setHeader('WWW-Authenticate', 'Basic realm=ejemplo');
            response.sendStatus(401);
            return;
        }
        //Autorización
        if( tipo && usuarioMG.tipo != tipo ){
            response.sendStatus(403);
            return;
        }
        //Todo bien
        try{
            callback(usuarioMG);
        } catch( excepcion){
            if( excepcion == "no autorizado"){
                response.sendStatus(403);
                return;
            }
            throw excepcion;
        }
    });

};

//logout
exports.logout = function(response){
    response.sendStatus(401);
}


