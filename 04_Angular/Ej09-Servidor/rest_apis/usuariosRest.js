const express = require("express");
const seguridadUtil = require("../util/seguridadUtil.js");
const negocioUsuarios = require("../modelo/negocio/negocioUsuarios.js");
const negocioClientes = require("../modelo/negocio/negocioClientes.js");
const negocioEmpleados = require("../modelo/negocio/negocioEmpleados.js");


let router = express.Router();

router.get("/usuarios/credenciales", function(request, response) {

    seguridadUtil.comprobarCredenciales(request, response, function(usuario) {
        response.json(usuario);
    });

    /*
    let login = request.query.login;
    let pw = request.query.pw;

    let promesa = negocioUsuarios.buscarPorLogin(login, pw);
    promesa.then( usuario => {
                        console.log("Buscar por login. Usuario:"+usuario);
                        if(usuario){
                            response.json(usuario); 
                        } else {
                            response.sendStatus(404);  
                        }
                    })
           .catch( err => response.sendStatus(500) );
    */
});

router.get("/logout", function(request, response) {
    seguridadUtil.logout(response);
});

//No
router.post("/clientes", function(request, response) {
    negocioClientes.insertar(request.body).
    then(ok => response.sendStatus(200)).
    catch(err => {
        console.log(err);
        response.sendStatus(500)
    });
});

//Si
router.put("/usuarios", function(request, response) {

    seguridadUtil.comprobarCredenciales(request, response, function(usuario) {
        negocioUsuarios.modificar(request.body).
        then(rs => response.sendStatus(200)).
        catch(err => {
            console.log(err);
            response.sendStatus(500);
        });
    }, "cliente");

});

//Si
router.post("/empleados", function(request, response) {
    negocioEmpleados.insertar(request.body).
    then(ok => response.sendStatus(200)).
    catch(err => response.sendStatus(500));
});

exports.router = router;