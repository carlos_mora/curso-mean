const express = require("express");
const seguridadUtil = require("../util/seguridadUtil.js");
const negocioIncidencias = require("../modelo/negocio/negocioIncidencias.js");

const router = express.Router();

/*
get incidencias/:id
get incidencias/clientes/:id
get incidencias/empleados/:id

post incidencias
put incidencias ¿?
delete incidencias
*/

router.get("/incidencias/:id", function(req, res){
    
    seguridadUtil.comprobarCredenciales(req, res, function(usuario){
        let id = req.params.id;
        negocioIncidencias.buscar(id).
            then( incidencia => res.json(incidencia) ).
            catch( err => { console.log(err);
                            res.sendStatus(500) } );
    }, "empleado");
});

router.get("/incidencias/clientes/:id", function(req, res){
    
    seguridadUtil.comprobarCredenciales(req, res, function(usuario){
        let id = req.params.id;
        negocioIncidencias.listarPorCliente(id, usuario).
            then( incidencias => res.json(incidencias) ).
            catch( err => { console.log(err);
                            res.sendStatus(500) } );
    }, "cliente");
});

router.get("/incidencias/empleados/:id", function(req, res){

    seguridadUtil.comprobarCredenciales(req, res, function(usuario){
        let id = req.params.id;
        negocioIncidencias.listarPorEmpleado(id).
            then( incidencias => res.json(incidencias) ).
            catch( err => { console.log(err);
                            res.sendStatus(500) } );
    }, "empleado");
});

router.post("/incidencias", function(req, res){

    seguridadUtil.comprobarCredenciales(req, res, function(usuario){
        let incidencia = req.body;
        negocioIncidencias.insertar(incidencia).
            then( rs => res.sendStatus(200)).
            catch( err => { console.log(err);
                            res.sendStatus(500) });
    }, "cliente");
});

router.put("/incidencias", function(req, res){

    seguridadUtil.comprobarCredenciales(req, res, function(usuario){
        let incidencia = req.body;
        negocioIncidencias.modificar(incidencia).
            then( rs => res.sendStatus(200)).
            catch( err => { console.log(err);
                            res.sendStatus(500) });
    }, "empleado");
});

exports.router = router;




