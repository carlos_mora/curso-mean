const Usuario = require("../entidades/usuario.js").Usuario; 
const Empleado = require("../entidades/empleado.js").Empleado; 
const Incidencia = require("../entidades/incidencia.js").Incidencia; 

//nombre
//login
//pw
//idioma
//sueldo
//codEmp
//dpto
exports.insertar = function(datosEmpleado){

    let usuarioMG = new Usuario(datosEmpleado);
    
    return usuarioMG.save().
    then( usuarioMG => {
        let empleadoMG = new Empleado(datosEmpleado);
        empleadoMG._id = usuarioMG._id;

        return empleadoMG.save()
    }).
    then( empleadoMG => {
        return true;
    });

}

exports.listarCargaTrabajo = function(){
    let empSinIncidencias;
    return Incidencia.distinct( 'empleado._id' ).
    then( empConIncidencias => {
        return Empleado.find( { _id : { $nin:empConIncidencias } }).lean().exec();
    }).
    then( rs => {
        empSinIncidencias = rs; 
        empSinIncidencias.forEach( e => e.incidencias=0 );
        return Incidencia.aggregate([
            {   $match: { estado : { $ne:'Finalizada'} } },
            {
                $group: { _id         : "$empleado._id",
                          nombre      : { $first : "$empleado.nombre" },
                          incidencias : {$sum : 1} }
            },
            {   $sort : { incidencias : 1 } } 
            ]);

    }).
    then( rs => {
        return empSinIncidencias.concat(rs);
    });
}

exports.modificar = function(usuario){
}

exports.borrar = function(_id){    
}

exports.buscar = function(_id){
}

exports.listar = function(){
}
