const negocioEmpleados = require("./negocioEmpleados.js");
const Incidencia = require("../entidades/incidencia.js").Incidencia;
const Cliente = require("../entidades/cliente.js").Cliente;
const Empleado = require("../entidades/empleado.js").Empleado;

exports.buscar = function(id){
    return Incidencia.findById(id);
}

exports.listarPorCliente = function(id, usuario){
    if(id != usuario._id){
        throw "no autorizado";
    }

    return Incidencia.find({ 'cliente._id' : id });
}

exports.listarPorEmpleado = function(id, usuario){
    if(id != usuario._id){
        throw "no autorizado";
    }

    return Incidencia.find({ 'empleado._id' : id });
}

exports.insertar = function(incidencia, usuario){

    if(usuario.tipo != "cliente"){
        throw "no autorizado";
    }

    let incidenciaMG = new Incidencia(incidencia);
    
    return Cliente.findById(incidencia.cliente._id).
    then( clienteMG => {
        incidenciaMG.cliente = clienteMG;
        return negocioEmpleados.listarCargaTrabajo();
    }).
    then( empleados => {
        //console.log("====================================");
        //console.log(empleados);
        let id = empleados[0]._id;
        return Empleado.findById(id);
    }).
    then( empleadoMG => {
        incidenciaMG.empleado = empleadoMG;
        //console.log("====================================");
        //console.log("(Insertar incidencia) Empleado:"+empleadoMG.nombre);
        return incidenciaMG.save();
    }).
    then( incidenciaMG => {
        return true;
    });

    /*El mismo proceso si todo fuera síncrono:
    let incidenciaMG = new Incidencia(incidencia);
    let clienteMG = Cliente.findById(incidencia.cliente._id).
    incidenciaMG.cliente = clienteMG;
    let empleados = negocioEmpleados.listarCargaTrabajo();
    let id = empleados[0]._id;
    let empleadoMG = Empleado.findById(id);
    incidenciaMG.empleado = empleadoMG;
    incidenciaMG.save();
    */

}

exports.modificar = function(incidencia){
    return Incidencia.findByIdAndUpdate(incidencia._id, incidencia);
}


