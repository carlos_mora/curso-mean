const Usuario = require("../entidades/usuario.js").Usuario; // :(
const Cliente = require("../entidades/cliente.js").Cliente; // :(
//const Usuario = require("/modelo/entidades/usuario.js").Usuario; // :) 

exports.insertar = function(usuario){
    let usuarioMG = new Usuario(usuario);
    usuarioMG.tipo = 'cliente';

    return usuarioMG.save().
    then(function(usuarioMG){
        let cliente = {
            '_id' : usuarioMG._id,
            'nombre' : usuarioMG.nombre,
            'cuentaBancaria' : null
        }
        let clienteMG = new Cliente(cliente);
        return clienteMG.save();
    }).
    then(function(clienteMG){
        return true;
    });    
}

exports.modificar = function(usuario){
    return Usuario.findByIdAndUpdate(usuario._id, usuario);
}

exports.borrar = function(_id){
    
    /*Con callbacks
    //si lo hacemos con callbacks la funcion 'borrar' no devuelve nada
    Usuario.findById(_id, function(err, usr){
        if(err) { throw err; }
        usr.remove(function(err,usr){
            if(err) { throw err; }
            console.log("Eliminado");            
        });
    });
    */

    /*Con promesas
    //Es la manera en la que lafunción borrar puede devolver algo, aunque
    //sea la promesa de que el usuario se va a borrar...
    return Usuario.findById(_id).then(function(usr){
        return usr.remove();
    }).then(function(rs){
        //
        return "Borrado";
    });
    */      

    //Si sabemos que borramos elusuario si o si:
    return Usuario.findByIdAndRemove(_id);
}

exports.buscar = function(_id){
    return Usuario.findById(_id); //La promesa de un usuario
}

exports.listar = function(){
    return Usuario.find({}); //La promesa de un array con cero o más usuarios
}
