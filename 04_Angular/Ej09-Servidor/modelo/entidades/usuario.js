const mongoose = require("mongoose");

let esquemaUsuario = new mongoose.Schema({
    nombre : String,
    login  : String,
    pw     : String,
    idioma : String,
    tipo   : String
});

exports.Usuario = mongoose.model('usuarios', esquemaUsuario);

/*
//////////////////////////////////////////////////////
const mongo = require("mongodb");
function Cliente(){
   this.nombre = undefined; 
   this.direccion = undefined; 
   this.telefono = undefined; 
}
Cliente.find= function(id){
    console.log("Buscando el cliente:"+id)
}
Cliente.prototype.save = function(){
    that = this;
    mongo.connect("mongodb://localhost:27017/HABER", function(err, db){
        if(err) { throw err }
        db.collection("clientes").insertOne(that, function(err, rs){
            if( err ) { throw err }
            console.log("Insertado");
        })
    })
}
let c = new Cliente();
c.nombre = 'John McClane';
c.save();
Cliente.find(8);
//////////////////////////////////////////////////////
*/
