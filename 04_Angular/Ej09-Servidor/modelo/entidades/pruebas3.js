const mongoose   = require("mongoose");
const Usuario    = require("./usuario.js").Usuario;
const Cliente    = require("./cliente.js").Cliente;
const Empleado   = require("./empleado.js").Empleado;
const Incidencia = require("./incidencia.js").Incidencia;

const url = "mongodb://localhost:27017/incidencias"
mongoose.Promise = global.Promise;
mongoose.connect(url, { useMongoClient:true });


/*
Creada
Asignada
Finalizada
*/

//buscar empleados que no tengan incidencias
function listarCargaTrabajo(){

    let empSinIncidencias;
    return Incidencia.distinct( 'empleado._id' ).
    then( empConIncidencias => {


        return Empleado.find( { _id : { $nin:empConIncidencias } }).lean().exec();
    }).
    then( rs => {
        empSinIncidencias = rs;        
        empSinIncidencias.forEach( e => e.incidencias=0 );
        return Incidencia.aggregate([
            {   $match: { estado : { $ne:'Finalizada'} } },
            {
                $group: { _id         : "$empleado._id",
                          nombre      : { $first : "$empleado.nombre" },
                          incidencias : {$sum : 1} }
            },
            {   $sort : {incidencias:1} } 
            ]);
    }).
    then( rs => {
        let resultado = empSinIncidencias.concat(rs);
        return resultado;
    });

}


listarCargaTrabajo().
then( rs => console.log(rs) ).
catch( err => console.log(err) )