const mongoose = require("mongoose");
const Cliente = require("./cliente.js").Cliente;
const Empleado = require("./empleado.js").Empleado;


var fechaDesdeAhora = function(horas, dias){
    if(!horas) { horas = 0 }
    if(!dias) { dias = 0 }
    var fecha = new Date();
    fecha.setTime(fecha.getTime() + (3600*1000*horas) + (24*3600*1000*dias));
    return fecha;
};

let esquemaIncidencia = new mongoose.Schema({
    asunto          : String,
    descripcion     : String,
    fechaAlta       : { type: Date, default: fechaDesdeAhora(1,0) }, 
    fechaLimite     : { type:Date, default: fechaDesdeAhora(1,3) },
    fechaResolucion : Date,
    prioridad       : String,
    estado          : { type: String, default: 'Creada' },

    cliente  : Cliente.schema,
    empleado : Empleado.schema
});

exports.Incidencia = mongoose.model('incidencias', esquemaIncidencia);
