const mongoose   = require("mongoose");
const Usuario    = require("./usuario.js").Usuario;
const Cliente    = require("./cliente.js").Cliente;
const Empleado   = require("./empleado.js").Empleado;
const Incidencia = require("./incidencia.js").Incidencia;

const url = "mongodb://localhost:27017/incidencias"
mongoose.Promise = global.Promise;
mongoose.connect(url, { useMongoClient:true });

let usr = {
    nombre : 'Harry Callahan',
    login  : 'a',
    pw     : 'a',
    idioma : 'EN'
}
usr.tipo = 'empleado';

let emp = {
    _id : null,
    nombre : usr.nombre,
    sueldo : 20000
}

/*
let usuarioMG = new Usuario(usr);
usuarioMG.save().
then( usuarioMG => {
    emp._id = usuarioMG._id;
    let empleadoMG = new Empleado(emp);
    return empleadoMG.save();
}).
then( xxx => console.log("YA"));
*/

/*
//5a0de345cab75b0570378d50
let incidenciaMG = null;
Incidencia.findById('5a0de345cab75b0570378d50').
then( incMG => {
    //5a130ed12b081c14c8f095f7

    incidenciaMG = incMG;
    return Empleado.findById("5a130ed12b081c14c8f095f7");
}).
then( empleadoMG => {
    incidenciaMG.empleado = empleadoMG;
    console.log(incidenciaMG);
    return incidenciaMG.save();
}).
then( incMG => {
    console.log("YA");
})
*/

let incidenciaMG = new Incidencia();
incidenciaMG.save().then( i => console.log(i) );

