import { Ej08LoginPage } from './app.po';

describe('ej08-login App', function() {
  let page: Ej08LoginPage;

  beforeEach(() => {
    page = new Ej08LoginPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
