import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRutasModule } from 'app/app.rutas.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './componentes/login/login.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { CuentaComponent } from './componentes/cuenta/cuenta.component';
import { AplicacionComponent } from './componentes/aplicacion/aplicacion.component';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { AltaUsuarioComponent } from './componentes/alta-usuario/alta-usuario.component';
import { AceptacionCondicionesComponent } from './componentes/aceptacion-condiciones/aceptacion-condiciones.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InicioComponent,
    CuentaComponent,
    AplicacionComponent,
    AltaUsuarioComponent,
    AceptacionCondicionesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, 
    AppRutasModule
  ],
  providers: [
    SesionService, 
    UsuariosService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
