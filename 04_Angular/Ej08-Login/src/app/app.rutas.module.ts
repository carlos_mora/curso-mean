import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './componentes/login/login.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { CuentaComponent } from './componentes/cuenta/cuenta.component';
import { AplicacionComponent } from './componentes/aplicacion/aplicacion.component';
import { AltaUsuarioComponent } from 'app/componentes/alta-usuario/alta-usuario.component';
import { AceptacionCondicionesComponent } from 'app/componentes/aceptacion-condiciones/aceptacion-condiciones.component';

// Rutas para el router-outlet presente en AplicacionComponent
const rutasAplicacion:Routes = [
	{
		'path' : 'inicio', 
		'component' : InicioComponent
	},
	{
		'path' : 'cuenta', 
		'component' : CuentaComponent
	}
];

// Rutas para el router-outlet presente en AppComponent
const rutas:Routes = [
	{
		'path' : '', 
		'component' : LoginComponent
	},
	{
		'path' : 'login', 
		'component' : LoginComponent
	},
	{
		'path' : 'alta', 
		'component' : AltaUsuarioComponent
	},
	{
		'path' : 'aceptacion', 
		'component' : AceptacionCondicionesComponent
	},
	{
		'path' : 'aplicacion', 
		'component' : AplicacionComponent, 
		'children' : rutasAplicacion
	}
];

@NgModule({
  imports: [ RouterModule.forRoot(rutas) ], 
	exports : [ RouterModule ]
})
export class AppRutasModule { }
