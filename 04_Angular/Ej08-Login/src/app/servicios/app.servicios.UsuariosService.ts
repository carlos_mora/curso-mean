import { Injectable } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class UsuariosService {

  private url:string = "http://localhost:4321";

  constructor(private http:Http) { 
    
  }


  public buscarPorLogin(usuario:Usuario):Observable<Usuario>{
    let parametros:string = "?login="+usuario.login+"&pw="+usuario.pw; 
    return this.http.get(this.url+"/usuarios/credenciales"+parametros)
      .map(response => response.json());
  }

  public insertarUsuario(usuario:Usuario):Observable<Response>{
    let headers:Headers = new Headers({ 'Content-Type' : 'Application/json'});
    return this.http.post(this.url+"/usuarios", 
                        JSON.stringify(usuario), 
                        {'headers' : headers});
  }

  public modificarUsuario(usuario:Usuario):Observable<Response>{
    let headers:Headers = new Headers({ 'Content-Type' : 'Application/json'});
    return this.http.put(this.url+"/usuarios", 
                        JSON.stringify(usuario), 
                        {'headers' : headers});
  }


}
