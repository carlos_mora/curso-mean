import { Injectable } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class UsuariosService {
    private url = 'http://localhost:4321';
    constructor(private http: Http) {}

    public login(usuario: Usuario): Observable<Usuario> {
        const parametros: string =
            '?login=' + usuario.login + '&pw=' + usuario.pw;
        return this.http
            .get(this.url + '/usuarios/credenciales' + parametros)
            .map(response => response.json());
    }

    public insertar(usuario: Usuario): Observable<Response> {
        const headers: Headers = new Headers({
            'content-type': 'application/json'
        });

        return this.http.post(this.url + '/usuarios', JSON.stringify(usuario), {
            headers: headers
        });
    }

    public listar(): Observable<Usuario[]> {
        return this.http
            .get(this.url + '/usuarios')
            .map(respuesta => respuesta.json());
    }

    public buscar(id: number): Observable<Usuario> {
        return this.http
            .get(this.url + '/usuarios/' + id)
            .map(respuesta => respuesta.json());
    }

    public modificar(usuario): Observable<Response> {
        const headers: Headers = new Headers({
            'content-type': 'application/json'
        });

        return this.http.put(this.url + '/usuarios', JSON.stringify(usuario), {
            headers: headers
        });
    }

    public borrar(usuario): Observable<Response> {
        return this.http.delete(this.url + '/usuarios/' + usuario.id);
    }
}
