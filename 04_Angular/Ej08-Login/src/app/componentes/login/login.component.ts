import { Component, OnInit } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

// Librerias externas a Angular. 
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { SesionService } from 'app/servicios/app.servicios.SesionService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public usuario:Usuario;
  public mensaje:string;
  
  constructor(private usuarioService:UsuariosService, 
              private http:Http, 
              private router:Router,
              private ss:SesionService) { 
    this.usuario = new Usuario(null, null, null, null, null);
  }

  ngOnInit() {
  }

  public login(){
    // Llamar al servicio
    this.usuarioService.buscarPorLogin(this.usuario).subscribe(
      data => {
        this.ss.add(data, "usuario");
        this.router.navigate(['/aplicacion/inicio']);
      }, 
      error => this.mensaje = "Usuario y contraseña incorrectos"
    );
    
  }

}
