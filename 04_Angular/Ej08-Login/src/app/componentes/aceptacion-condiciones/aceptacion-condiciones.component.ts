import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

// Librerias externas a Angular. 
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { Usuario } from 'app/entidades/entidades.usuario';
import { SesionService } from 'app/servicios/app.servicios.SesionService';

@Component({
  selector: 'app-aceptacion-condiciones',
  templateUrl: './aceptacion-condiciones.component.html',
  styleUrls: ['./aceptacion-condiciones.component.css']
})
export class AceptacionCondicionesComponent implements OnInit {

  private acepto:boolean;
  private user:Usuario;

  constructor(private usuariosService:UsuariosService, 
              private http:Http, 
              private router:Router, 
              private ss:SesionService) { 
                this.user = this.ss.get("usuario");
              }

  ngOnInit() {
  }

  public siguiente():void{
    if(!this.acepto){
      console.log("Debes aceptar las condiciones.");
      return;
    }
    
    
    this.usuariosService.insertarUsuario(this.user).subscribe(
      response => this.recargarUsuario(), 
      error => console.log("ZAS al insertar: " + error)
    );
    
  }

  private recargarUsuario(){
    this.usuariosService.buscarPorLogin(this.user).subscribe(
      data => {
        this.ss.add(data, "usuario");
        this.router.navigate(['/aplicacion/inicio']);
      }, 
      error => console.log(error)
    );
  }


}
