import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';

@Component({
  selector: 'app-aplicacion',
  templateUrl: './aplicacion.component.html',
  styleUrls: ['./aplicacion.component.css']
})
export class AplicacionComponent implements OnInit {

  public usuario:Usuario;

  constructor(private ss:SesionService) { 
    this.usuario = ss.get("usuario");
    //console.log(this.usuario);
  }

  ngOnInit() {
  }

}
