import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  private usuario: Usuario;

  constructor(private ss: SesionService) {
    this.usuario = ss.get('usuario');
    console.log(this.usuario);
  }

  ngOnInit() {
  }

}
