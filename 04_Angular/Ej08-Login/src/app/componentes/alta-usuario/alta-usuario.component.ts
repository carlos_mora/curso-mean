import { Component, OnInit } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Router } from '@angular/router';
import { SesionService } from 'app/servicios/app.servicios.SesionService';

@Component({
  selector: 'app-alta-usuario',
  templateUrl: './alta-usuario.component.html',
  styleUrls: ['./alta-usuario.component.css']
})
export class AltaUsuarioComponent implements OnInit {

  private usuario:Usuario;
  private pw2:string;
  private mensaje:string;
  
  constructor(private sesionService:SesionService, private router:Router) { 
    this.usuario = new Usuario(null, null, null, null, null);
  }

  ngOnInit() {
  }

  public siguiente():void{
    let u = this.usuario;
    if(u.nombre && u.login && u.pw && u.idioma && u.pw == this.pw2){
      console.log('Todo correcto, palante');
      this.sesionService.add(this.usuario, 'usuario');
      this.router.navigate(['aceptacion']);
    } else {
      console.log('Rellena todos los campos, por favor');
      this.mensaje = 'Rellena todos los campos, por favor';
    }
    
  }

}
