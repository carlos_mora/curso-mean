let express = require("express");
let bodyParser = require("body-parser");
let app = express();

// app.use(express.static('06_recursos_estaticos'));

app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.header(
        "Access-Control-Allow-Methods",
        "POST, GET, PUT, DELETE, OPTIONS"
    );
    next();
});
app.disable("x-powered-by");

app.listen(4321, function() {
    console.log("Esperando peticiones...");
});

app.get("/usuarios/credenciales", function(request, response) {
    console.log(request.query.login);
    console.log(request.query.pw);
    let login = request.query.login;
    let pw = request.query.pw;
    if (login === "a" && pw === "a") {
        let usuario = {
            id: 1,
            nombre: "Harry Callahan",
            login: "a",
            pw: "a",
            idioma: "es"
        };
        response.json(usuario);
    } else {
        response.sendStatus(404);
    }
});

app.post("/usuarios", function(request, response) {});
app.put("/usuarios", function(request, response) {});
app.delete("/usuarios", function(request, response) {});
