import { Component, OnInit } from '@angular/core';
import { Incidencia } from "app/entidades/entidades.incidencia";
import { SesionService } from "app/servicios/app.servicios.SesionService";
import { IncidenciasService } from "app/servicios/app.servicios.IncidenciasService";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-form-incidencia-emp',
  templateUrl: './form-incidencia-emp.component.html',
  styleUrls: ['./form-incidencia-emp.component.css']
})
export class FormIncidenciaEmpComponent implements OnInit {

  public incidencia:Incidencia;

  constructor(private sesionService:SesionService,
              private incidenciasService:IncidenciasService,
              private ruta:ActivatedRoute,
              private router:Router) { 

    this.incidencia = new Incidencia();
    
    let id:string = ruta.snapshot.params['id'];
    incidenciasService.buscar(id).
      subscribe( incidencia => this.incidencia = incidencia, 
                 err => console.log(err) );

  }
  
  ngOnInit() {
  }

  public modificar(){
    this.incidenciasService.modificar(this.incidencia).
      subscribe( rs => this.router.navigate( [ '/appEmpleado/inicio' ]))
  }

}
