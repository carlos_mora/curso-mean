import { SesionService } from "app/servicios/app.servicios.SesionService";
import { Injectable } from "@angular/core";
import { Usuario } from "app/entidades/entidades.usuario";
import { Headers } from "@angular/http";

@Injectable()
export class SeguridadUtil {

    public headerAuth     : Headers;
    public headerAuthJson : Headers;

    constructor(private sesionService:SesionService){
        //Si SeguridadUtil es global y se crea al arrancar la app no tiene
        //sentido invocar 'crearHeaders' sin usuario en el sesionService
        //this.crearHeaders();
    }

    public crearHeaders(){
        this.getAuthenticationHeader();
        this.getJsonAuthenticationHeaderOnTheNight();
    }

    private getAuthenticationHeader():void{
        let usuario:Usuario = this.sesionService.get("usuario");
        let token:String = "Basic " + btoa( usuario.login+":"+usuario.pw );
        this.headerAuth = new Headers( { 'Authorization': token } );
    }

    private getJsonAuthenticationHeaderOnTheNight():void{
        let usuario:Usuario = this.sesionService.get("usuario");
        let token:String = "Basic " + btoa( usuario.login+":"+usuario.pw );
        this.headerAuthJson = new Headers( { 'Authorization': token } );
        this.headerAuthJson.append( 'Content-type', 'Application/json' );
    }    

}