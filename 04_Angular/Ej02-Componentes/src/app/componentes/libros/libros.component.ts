import { Component, OnInit } from '@angular/core';
import { Libro } from 'app/entidades/app.entidades.libro';
import { LibrosService } from 'app/servicios/libros.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {
  public libros: Libro[];
  public libroSel: Libro;
  // ServicioLibros es una dependencia 
  private librosService: LibrosService;

  // Los componentes son instanciados por el FW
  constructor() {
    console.log('LibrosComponent:constructor');
    // this.libroSel = new Libro("", "", "", 0);
    this.librosService = new LibrosService();
    this.vaciarFormulario();
    this.actualizarTabla();
  }

  ngOnInit() {
    console.log('LibrosComponent:ngOnInit');
  }

  public actualizarTabla(){
    this.libros = this.librosService.listarLibros();
  }

  public insertarLibro(): void {
    this.librosService.insertarLibro(this.libroSel);
    this.actualizarTabla();
  }
  public modificarLibro(): void {
    console.log('LibrosComponent:modificarLibro');
    this.librosService.modificarLibro(this.libroSel);
    this.actualizarTabla();
  }
  public borrarLibro(): void {
    console.log('LibrosComponent:borrarLibro');
    this.librosService.borrarLibro(this.libroSel);
    this.actualizarTabla();
  }
  public vaciarFormulario(): void {
    // this.libroSel = new Libro("", "", "", 0);
    this.libroSel = new Libro(null,null, null, null, null);
  }
  public seleccionarLibro(libro:Libro): void {
    this.libroSel = this.librosService.buscarLibro(libro.id);
  }

}
