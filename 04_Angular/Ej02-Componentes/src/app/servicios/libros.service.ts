import {
  Injectable
} from '@angular/core';
import {
  Libro
} from 'app/entidades/app.entidades.libro';

// @Injectable()
export class LibrosService {
  private libros: Libro[] = [];
  private contador = 3;
  constructor() {
    this.libros.push(new Libro(1, "El Quijote", "Cervantes", "1234", 15));
    this.libros.push(new Libro(2, "El corazón de las tinieblas", "J. Conrad", "3210", 16));
    this.libros.push(new Libro(3, "Tiempo anclado", "Julian Escobar", "9732", 10));
  }

  public insertarLibro(libro: Libro): void {
    let x = JSON.parse(JSON.stringify(libro));
    this.contador++;
    x.id = this.contador;
    console.log(JSON.stringify(libro));
    this.libros.push(x);
  }

  public listarLibros(): Libro[] {
    let lista: Libro[] = [];

    for (let i = 0; i < this.libros.length; i++) {
      let l = this.libros[i];
      // console.log(l);
      lista.push(new Libro(l.id, l.titulo, l.autor, l.ISBN, l.precio));
    }
    return lista;

    /*
    let result = this.libros.slice(0);
    result.map( function( l, i, a ) { result[i] = Object.create(l)} );
    return result;
    */

  }

  public buscarLibro(id): Libro {
    for (let l of this.libros) {
      if (l.id == id) {
        return new Libro(l.id, l.titulo, l.autor, l.ISBN, l.precio);
      }
    }
    return null;
  }

  public modificarLibro(libro): void {
    for (let l of this.libros) {
      if (l.id == libro.id) {
        l.titulo = libro.titulo;
        l.autor = libro.autor;
        l.ISBN = libro.ISBN;
        l.precio = libro.precio;
        break;
      }
    }
  }
  
  public borrarLibro(libro): void {
    for (let i = 0; i < this.libros.length; i++) {
      let l = this.libros[i];
      if (l.id == libro.id) {
        this.libros.splice(i, 1);
        break;
      }
    }
  }

}
