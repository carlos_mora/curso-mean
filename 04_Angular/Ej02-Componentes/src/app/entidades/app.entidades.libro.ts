// Nombres de las clases en UppeCamelCase
export class Libro {
  // no usamos privates ni accesores
  constructor(
    public id: number,
    public titulo: string,
    public autor: string,
    public ISBN: string,
    public precio: number,
  ) {

  }

  // toString
  public toString():string {
    return this.id+', '
    +this.titulo+', '
    +this.autor+', '
    +this.ISBN+', '
    +this.precio;
  }
}
