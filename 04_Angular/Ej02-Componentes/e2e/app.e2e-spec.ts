import { Ej02ComponentesPage } from './app.po';

describe('ej02-componentes App', function() {
  let page: Ej02ComponentesPage;

  beforeEach(() => {
    page = new Ej02ComponentesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
