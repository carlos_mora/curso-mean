import { Ej09MagicoFinDeFiestaPage } from './app.po';

describe('ej09-magico-fin-de-fiesta App', function() {
  let page: Ej09MagicoFinDeFiestaPage;

  beforeEach(() => {
    page = new Ej09MagicoFinDeFiestaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
