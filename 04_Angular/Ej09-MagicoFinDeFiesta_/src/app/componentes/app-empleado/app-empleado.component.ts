import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'appEmpleado',
  templateUrl: './app-empleado.component.html',
  styleUrls: ['./app-empleado.component.css']
})

export class AppEmpleadoComponent  implements OnInit {

  public usuario:Usuario;

  constructor(private sesionService:SesionService,
    private router: Router ) { 
    this.usuario = sesionService.get("usuario");
  }

  ngOnInit() {
  }
  public logout(){
    this.sesionService.remove('usuario');
    this.router.navigate( [ '/login' ]);

  };
}
