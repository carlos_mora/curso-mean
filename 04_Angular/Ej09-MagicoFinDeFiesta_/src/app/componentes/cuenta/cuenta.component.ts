import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.css']
})
export class CuentaComponent implements OnInit {

  public usuario;
  public confirmacionPW;
  constructor(private sesionService: SesionService,
  private usuarioService: UsuariosService) {
    this.usuario = this.sesionService.get('usuario');
    this.confirmacionPW = this.usuario.pw;
   }

  ngOnInit() {
  }

  public guardar(){
    console.log(this.usuario);
    this.usuarioService.modificar(this.usuario)
      .subscribe( rs => {
        console.log('ok');
      },
        err => {
          console.log(err);
        })
  }

}
