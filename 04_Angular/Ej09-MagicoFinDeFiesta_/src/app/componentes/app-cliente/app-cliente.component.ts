import { Component, OnInit } from '@angular/core';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Incidencia } from 'app/entidades/entidades.incidencia';
import { IncidenciasService } from 'app/servicios/app.servicios.IncidenciasService';
import { Router } from '@angular/router';

@Component({
  selector: 'appCliente',
  templateUrl: './app-cliente.component.html',
  styleUrls: ['./app-cliente.component.css']
})

export class AppClienteComponent  implements OnInit {

  public usuario: Usuario;

  constructor(private sesionService: SesionService,
    private router: Router ) {
    this.usuario = sesionService.get('usuario');
  }

  ngOnInit() {
  }

  // tslint:disable-next-line:one-line
  public logout(){
    this.sesionService.remove('usuario');
    this.router.navigate( [ '/login' ]);

  };
}
