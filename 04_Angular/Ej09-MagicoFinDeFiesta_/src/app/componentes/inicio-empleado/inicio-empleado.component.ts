import { Component, OnInit } from '@angular/core';

import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Usuario } from 'app/entidades/entidades.usuario';
import { Incidencia } from 'app/entidades/entidades.incidencia';
import { IncidenciasService } from 'app/servicios/app.servicios.IncidenciasService';

@Component({
  selector: 'app-inicio-empleado',
  templateUrl: './inicio-empleado.component.html',
  styleUrls: ['./inicio-empleado.component.css']
})
export class InicioEmpleadoComponent implements OnInit {

  public usuario: Usuario;
  public incidencias: Incidencia[];

  constructor(private sesionService: SesionService,
              private incidenciasService: IncidenciasService) {
    this.usuario = sesionService.get('usuario');
    this.obtenerIncidencias();
  }

  ngOnInit() {
  }
  public obtenerIncidencias(): void {
    console.log('-----------');
    this.incidenciasService.listarPorEmpleado(this.usuario._id)
      .subscribe( incidencias => this.incidencias = incidencias,
          error => console.log(error));
  }

}
