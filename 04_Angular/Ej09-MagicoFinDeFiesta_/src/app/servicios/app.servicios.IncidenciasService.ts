import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Incidencia } from 'app/entidades/entidades.incidencia';
import { SeguridadUtil } from 'app/util/app.util.seguridadUtil';

@Injectable()
export class IncidenciasService {

    private url = 'http://localhost:4321';
    private headAuth: Headers;
    private headAuthJSON: Headers;
    
    constructor (private http: Http, private seguridadUtil: SeguridadUtil) {
        this.headAuth = this.seguridadUtil.getAuthenticationHeader();
        this.headAuthJSON = this.seguridadUtil.getJsonHeader();
    }

    public listarPorCliente(_id: String): Observable<Incidencia[]> {
        return this.http.get(this.url + '/incidencias/clientes/' + _id, {headers: this.headAuth})
            .map(response => response.json());
    }

    public buscar(_id: String): Observable<Incidencia> {
        return this.http.get(this.url + '/incidencias/' + _id, {headers: this.headAuth})
            .map(response => response.json());
    }

    public insertar(incidencia: Incidencia): Observable<Response> {
        return this.http.post(this.url + '/incidencias',
                         JSON.stringify(incidencia),
                         { 'headers': this.headAuthJSON } );
    }

    public listarPorEmpleado(_id: String): Observable<Incidencia[]> {
        const headers : Headers = this.seguridadUtil.getAuthenticationHeader():
        return this.http.get(this.url + '/incidencias/empleados/' + _id, {headers: headers})
            .map(response => response.json());
    }



    /*
    public modificar(incidencia: Incidencia): Observable<Response>{
        const headers: Headers = new Headers( { 'Content-Type': 'application/json' } );
        return this.http.put(this.url + '/incidencias',
                         JSON.stringify(incidencia),
                         { 'headers': headers } );
    }

    public borrar(incidencia: Incidencia): void{
        //
    }
*/
}
