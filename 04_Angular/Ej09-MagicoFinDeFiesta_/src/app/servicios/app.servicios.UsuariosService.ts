import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Usuario } from 'app/entidades/entidades.usuario';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class UsuariosService{

    private url = 'http://localhost:4321';

    constructor(private http: Http){
    }

    public buscarPorLogin(usuario: Usuario): Observable<Usuario> {
        const parametros: string = '?login=' + usuario.login + '&pw=' + usuario.pw;
        const token: string = ' Basic ' + btoa(usuario.login + ':' + usuario.pw);
        const headers: Headers = new Headers( { 'Authorization': token } );
        return this.http.get(this.url + '/usuarios/credenciales' + parametros,
        { 'headers': headers })
            .map(response => response.json());
    }

    public insertar(usuario: Usuario): Observable<Response> {
        const headers: Headers = new Headers( { 'Content-Type': 'application/json' } );
        return this.http.post(this.url + '/clientes',
                         JSON.stringify(usuario),
                         { 'headers': headers } );
    }

    public modificar(usuario: Usuario): Observable<Response>{
        const headers: Headers = new Headers( { 'Content-Type': 'application/json' } );
        console.log(JSON.stringify(usuario));
        return this.http.put(this.url + '/usuarios',
                         JSON.stringify(usuario),
                         { 'headers': headers } );
    }

    public borrar(usuario: Usuario): void{
        //
    }

}