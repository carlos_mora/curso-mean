import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRutasModule } from 'app/app.rutas.module';

import { AppComponent } from './app.component';
import { LoginComponent } from 'app/componentes/login/login.component';
import { CuentaComponent } from 'app/componentes/cuenta/cuenta.component';
import { AppClienteComponent } from 'app/componentes/app-cliente/app-cliente.component';
import { AppEmpleadoComponent } from 'app/componentes/app-empleado/app-empleado.component';
import { UsuariosService } from 'app/servicios/app.servicios.UsuariosService';
import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { AltaUsuarioComponent } from './componentes/alta-usuario/alta-usuario.component';
import { AceptacionCondicionesComponent } from './componentes/aceptacion-condiciones/aceptacion-condiciones.component';
import { InicioClienteComponent } from './componentes/inicio-cliente/inicio-cliente.component';
import { IncidenciasService } from 'app/servicios/app.servicios.IncidenciasService';
import { FormIncidenciaCliComponent } from './componentes/form-incidencia-cli/form-incidencia-cli.component';
import { InicioEmpleadoComponent } from './componentes/inicio-empleado/inicio-empleado.component';

@NgModule({
  declarations: [
    LoginComponent,
    CuentaComponent,
    AppClienteComponent,
    AppEmpleadoComponent,
    AppComponent,
    AltaUsuarioComponent,
    AceptacionCondicionesComponent,
    InicioClienteComponent,
    InicioClienteComponent,
    FormIncidenciaCliComponent,
    InicioEmpleadoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRutasModule
  ],
  providers: [ SesionService,
               UsuariosService,
              IncidenciasService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
