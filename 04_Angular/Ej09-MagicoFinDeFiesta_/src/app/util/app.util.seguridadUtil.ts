import { SesionService } from 'app/servicios/app.servicios.SesionService';
import { Injectable, Headers } from '@angular/core';
import { Usuario } from 'app/entidades/entidades.usuario';

@Injectable()
export class SeguridadUtil {
    constructor(private sesionService: SesionService) {

    }
    public getAuthenticationHeader(): Headers {
        const usuario: Usuario = this.sesionService.get('usuario');
        const token: string = ' Basic ' + btoa(usuario.login + ':' + usuario.pw);
        return new Headers({ 'Authorization': token });
    }
    public getJsonHeader(): Headers {
        const headers: Headers = this.getAuthenticationHeader();
        headers.append('Content-Type', 'application/json') ;
        return headers;
    }
}
