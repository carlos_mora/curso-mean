
export class Incidencia {
    public _id: number;
    public asunto: string;
    public descripcion: string;
    public fechaAlta: string;
    public fechaLimite: string;
    public fechaResolucion: string;
    public prioridad: string;
    public estado: string;
    public cliente: any;
    public empleado: any;
    
    constructor() {
    }
}
