
export class Usuario {
    constructor(public _id: string,
                public nombre: string,
                public login: string,
                public pw: string,
                public idioma: string,
                public tipo: string
                ) {
    }
}
